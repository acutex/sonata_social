<?php

namespace Mfarm\LocationBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Mfarm\LocationBundle\Model\DistrictInterface;
use Mfarm\LocationBundle\Form\DistrictType;
use Mfarm\LocationBundle\Exception\InvalidFormException;


/**
 * District Handler 
 * @DI\Service("locationbundle.district.handler")
 */
      
class DistrictHandler implements DistrictHandlerInterface {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory)
  {
      $this->om = $om;
      $this->entityClass = "Mfarm\LocationBundle\Entity\District";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
  }

  /**
   * Get a District.
   *
   * @param mixed $id
   *
   * @return DistrictInterface
   */
  public function get($id)
  {
      return $this->repository->find($id);
  }

  /**
   * Get a list of Districts.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0)
  {
      return $this->repository->findAll();
  }
  
  /**
   * Get a list of Farmers.
   *
   * @return array
   */
  public function allDataQuery()
  {
      return $this->repository->getAll();
  }

  /**
   * Create a new District.
   *
   * @param array $parameters
   *
   * @return DistrictInterface
   */
  public function post(array $parameters)
  {
      $district = $this->createDistrict();

      return $this->processForm($district, $parameters, 'POST');
  }

  /**
   * Edit a District.
   *
   * @param DistrictInterface $district
   * @param array         $parameters
   *
   * @return DistrictInterface
   */
  public function put(DistrictInterface $district, array $parameters)
  {
    return $this->processForm($district, $parameters, 'PUT');
  }

  /**
   * Partially update a District.
   *
   * @param DistrictInterface $district
   * @param array         $parameters
   *
   * @return DistrictInterface
   */
  public function patch(DistrictInterface $district, array $parameters)
  {
      return $this->processForm($district, $parameters, 'PATCH');
  }

  /**
   * Delete a District.
   *
   * @return boolean
   */
  public function delete($id)
  {
      $entity = $this->repository->find($id);
      if(is_object($entity)){
        $this->om->remove($entity);
        $this->om->flush();
        return true;
      }else{
        return false;
      }
  }

  /**
   * Processes the form.
   *
   * @param DistrictInterface $district
   * @param array         $parameters
   * @param String        $method
   *
   * @return DistrictInterface
   *
   * @throws Mfarm\LocationBundle\Exception\InvalidFormException
   */
  private function processForm(DistrictInterface $district, array $parameters, $method = "PUT")
  {
      $form = $this->formFactory->create(new DistrictType(), $district, array('method' => $method));
      if(array_key_exists('_method', $parameters))
        unset($parameters['_method']);
      $form->submit($parameters, 'PATCH' !== $method);
      if ($form->isValid()) {

          $district = $form->getData();
          $this->om->persist($district);
          $this->om->flush($district);

          return $district;
      }

      throw new InvalidFormException('Invalid submitted data', $form, $district);
  }

  private function createDistrict()
  {
      return new $this->entityClass();
  }
  
  public function getDataForSync(){
    if(array_key_exists('softdeleteable',$this->om->getFilters()->getEnabledFilters())){
        $this->om->getFilters()->disable('softdeleteable');
    }
    return$this->repository->getDataForSync();
  }
}
