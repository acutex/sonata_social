<?php

namespace Mfarm\LocationBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Mfarm\LocationBundle\Model\TownInterface;
use Mfarm\LocationBundle\Form\TownType;
use Mfarm\LocationBundle\Exception\InvalidFormException;


/**
 * Town Handler 
 * @DI\Service("locationbundle.town.handler")
 */
      
class TownHandler implements TownHandlerInterface {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory)
  {
      $this->om = $om;
      $this->entityClass = "Mfarm\LocationBundle\Entity\Town";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
  }

  /**
   * Get a Town.
   *
   * @param mixed $id
   *
   * @return TownInterface
   */
  public function get($id)
  {
      return $this->repository->find($id);
  }

  /**
   * Get a list of Towns.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0)
  {
      return $this->repository->findAll();
  }
  
  /**
   * Get a list of Farmers.
   *
   * @return array
   */
  public function allDataQuery()
  {
      return $this->repository->getAll();
  }

  /**
   * Create a new Town.
   *
   * @param array $parameters
   *
   * @return TownInterface
   */
  public function post(array $parameters)
  {
      $town = $this->createTown();

      return $this->processForm($town, $parameters, 'POST');
  }

  /**
   * Edit a Town.
   *
   * @param TownInterface $town
   * @param array         $parameters
   *
   * @return TownInterface
   */
  public function put(TownInterface $town, array $parameters)
  {
    return $this->processForm($town, $parameters, 'PUT');
  }

  /**
   * Partially update a Town.
   *
   * @param TownInterface $town
   * @param array         $parameters
   *
   * @return TownInterface
   */
  public function patch(TownInterface $town, array $parameters)
  {
      return $this->processForm($town, $parameters, 'PATCH');
  }

  /**
   * Delete a Town.
   *
   * @return boolean
   */
  public function delete($id)
  {
      $entity = $this->repository->find($id);
      if(is_object($entity)){
        $this->om->remove($entity);
        $this->om->flush();
        return true;
      }else{
        return false;
      }
  }

  /**
   * Processes the form.
   *
   * @param TownInterface $town
   * @param array         $parameters
   * @param String        $method
   *
   * @return TownInterface
   *
   * @throws Mfarm\LocationBundle\Exception\InvalidFormException
   */
  private function processForm(TownInterface $town, array $parameters, $method = "PUT")
  {
      $form = $this->formFactory->create(new TownType(), $town, array('method' => $method));
      if(array_key_exists('_method', $parameters))
        unset($parameters['_method']);
      $form->submit($parameters, 'PATCH' !== $method);
      if ($form->isValid()) {

          $town = $form->getData();
          $this->om->persist($town);
          $this->om->flush($town);

          return $town;
      }

      throw new InvalidFormException('Invalid submitted data', $form, $town);
  }

  private function createTown()
  {
      return new $this->entityClass();
  }
  
  public function getDataForSync(){
    if(array_key_exists('softdeleteable',$this->om->getFilters()->getEnabledFilters())){
        $this->om->getFilters()->disable('softdeleteable');
    }
    return$this->repository->getDataForSync();
  }
}
