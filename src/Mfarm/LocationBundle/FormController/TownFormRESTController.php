<?php

namespace Mfarm\LocationBundle\FormController;

use Mfarm\LocationBundle\Entity\Town;
use Mfarm\LocationBundle\Form\TownType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * TownForm controller.
 * @RouteResource("Towns")
 */
class TownFormRESTController extends VoryxController {

    /**
     * Get a Town entity
     * @ApiDoc(
     *  resource=true,
     *  description="Town based on id",
     *  
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Town $entity) {
        return $entity;
    }

    /**
     * Get all Town entities.
     * @ApiDoc(
     *  resource=true,
     *  description="Get Update an Town entity",
     *  parameters={
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="timestamp", requirements="timestamp", description="filter for latest CRUD since last sync")
     * 
     * 
     * 
     */
    public function cgetAction(Request $request) {
        try {


            $options = array();
            $time = $request->get('timestamp');

            if ($time != '') {
                $timestampDate = date("Y-m-d H:i:s", $time);
                $options['time'] = $timestampDate;
            }
            return
                    $this->container->get('actor.api.handler')->getSpecifiedRecords($options, "LocationBundle:Town");
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a Town entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     * {"name"="region", "dataType"="integer", "required"=true, "description"="region id "},
     * {"name"="district", "dataType"="integer", "required"=true, "description"="district id "},
     *   }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request) {
        $entity = new Town();
        $form = $this->createForm(new TownType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return array('data' => $entity, 'status' => true);
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update a Town entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     * {"name"="region", "dataType"="integer", "required"=true, "description"="region id "},
     * {"name"="district", "dataType"="integer", "required"=true, "description"="district id "},
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, Town $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new TownType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return array('data' => $entity, 'status' => true);
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Partial Update to a Town entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, Town $entity) {
        return $this->putAction($request, $entity);
    }

    /**
     * Delete a Town entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Town entity",
     *  parameters={
      }
     * )
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function deleteAction(Request $request, Town $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return array('status' => true, 'data' => sprintf('entity with id %d removed', $entity->getId()));
            ;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
