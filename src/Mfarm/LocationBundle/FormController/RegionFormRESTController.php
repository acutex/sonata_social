<?php

namespace Mfarm\LocationBundle\FormController;

use Mfarm\LocationBundle\Entity\Region;
use Mfarm\LocationBundle\Form\RegionType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * RegionForm controller.
 * @RouteResource("Regions")
 */
class RegionFormRESTController extends VoryxController {

    /**
     * Get a Region entity
     * @ApiDoc(
     *  resource=true,
     *  description="Region based on id",
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Region $entity) {
        return $entity;
    }

    /**
     * Get all Region entities.
     * @ApiDoc(
     *  resource=true,
     *  description="list Region entity",
     *  parameters={
      }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="timestamp", requirements="timestamp", description="filter for latest CRUD since last sync")
     * 
     * 
     * 
     */
    public function cgetAction(Request $request) {

        try {
            $options = array();
            $time = $request->get('timestamp');

            if ($time != '') {
                $timestampDate = date("Y-m-d H:i:s", $time);
                $options['time'] = $timestampDate;
            }
            return $this->container->get('actor.api.handler')->getSpecifiedRecords($options, "LocationBundle:Region");
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Create an Country entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},

     *   }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction(Request $request) {
        $entity = new Region();
        $form = $this->createForm(new RegionType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return array('data' => $entity, 'status' => true);
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Update Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, Region $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new RegionType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return array('data' => $entity, 'status' => true);
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Partial Update to a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, Region $entity) {
        return $this->putAction($request, $entity);
    }

    /**
     * Delete a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
      }
     * )
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function deleteAction(Request $request, Region $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return array('status' => true, 'data' => sprintf('entity with id %d removed', $entity->getId()));
            ;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
