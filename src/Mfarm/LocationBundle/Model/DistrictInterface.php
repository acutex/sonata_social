<?php

namespace Mfarm\LocationBundle\Model;



interface DistrictInterface {

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName();
  

  /**
   * Get Name
   *
   * @return string 
   */
  public function setName( $name);
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function getCreatedAt();
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function setCreatedAt( $createdAt);
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function getUpdatedAt();
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function setUpdatedAt( $updatedAt);
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function getCountry();
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function setCountry(\Mfarm\LocationBundle\Entity\Country $country);
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function getRegion();
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function setRegion(\Mfarm\LocationBundle\Entity\Region $region);
  

  /**
   * Add towns
   *
   * @param Mfarm\LocationBundle\Entity\Town $towns
   * @return District
   */
  public function addTown(\Mfarm\LocationBundle\Entity\Town $towns);
  

  /**
   * Remove towns
   *
   * @param Mfarm\LocationBundle\Entity\Town $towns
   */
  public function removeTown(\Mfarm\LocationBundle\Entity\Town $towns);
  

  /**
   * Get Towns
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getTowns();
  
}
