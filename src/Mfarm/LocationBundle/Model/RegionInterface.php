<?php

namespace Mfarm\LocationBundle\Model;



interface RegionInterface {

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName();
  

  /**
   * Get Name
   *
   * @return string 
   */
  public function setName( $name);
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function getCreatedAt();
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function setCreatedAt( $createdAt);
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function getUpdatedAt();
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function setUpdatedAt( $updatedAt);
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function getCountry();
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function setCountry(\Mfarm\LocationBundle\Entity\Country $country);
  

  /**
   * Add districts
   *
   * @param Mfarm\LocationBundle\Entity\District $districts
   * @return Region
   */
  public function addDistrict(\Mfarm\LocationBundle\Entity\District $districts);
  

  /**
   * Remove districts
   *
   * @param Mfarm\LocationBundle\Entity\District $districts
   */
  public function removeDistrict(\Mfarm\LocationBundle\Entity\District $districts);
  

  /**
   * Get Districts
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getDistricts();
  
}
