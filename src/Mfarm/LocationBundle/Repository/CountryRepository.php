<?php

namespace Mfarm\LocationBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CountryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CountryRepository extends EntityRepository {

    public function getAll($options = array()) {
        $qb = $this->createQueryBuilder('pageentity');
        $qb->addSelect('pageentity');
        $result = $qb->getQuery();
        return $result;
    }

    public function getDataForSync() {
        $qb = $this->createQueryBuilder('entityData');
        $qb->addSelect('entityData');
        $result = $qb->getQuery()->execute();
        return $result;
    }

    // function to get country id by country code
    public function getCountryId($code) {
        $qb = $this->createQueryBuilder('a');
        $qb->addSelect('a.id');
        $qb->andWhere('a.countryCode = :code');
        $qb->setParameter('code', $code);
        $qb->setMaxResults(1);
        $result = $qb->getQuery()->getScalarResult();
        $ids = array_column($result, 'id');
        return $ids[0];
    }

}
