<?php

namespace Mfarm\LocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CountryType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('countryCode')
                ->add('smsPrice')
                ->add('voicePrice')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mfarm\LocationBundle\Entity\Country'
            , 'cascade_validation' => true,
            'csrf_protection' => false));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mfarm_locationbundle_country';
    }

}
