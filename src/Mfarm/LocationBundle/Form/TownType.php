<?php

namespace Mfarm\LocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TownType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('country')
            ->add('region')
            ->add('district')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mfarm\LocationBundle\Entity\Town' , 'cascade_validation' => true,
            'csrf_protection' => false));
            
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mfarm_locationbundle_town';
    }
}
