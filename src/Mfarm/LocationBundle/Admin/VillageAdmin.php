<?php

namespace Mfarm\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType;


class VillageAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('id')
                ->add('name')
                ->add('createdAt', 'doctrine_orm_date_range', array(), 'sonata_type_date_range_picker', array('field_options_start' => array('format' => 'yyyy-MM-dd'),
                    'field_options_end' => array('format' => 'yyyy-MM-dd'))
                )
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('town')
                ->add('createdAt')
                ->add('updatedAt')
                ->add('deletedAt')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                
                 ->add('country', 'sonata_type_model', array(), array('add' => 'list'))
                ->add('region', DependentFormsType::class, array('required' => false, 'entity_alias' => 'region_by_country'
                    , 'parent_field' => 'country', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
                ->add('district', DependentFormsType::class, array('required' => false, 'entity_alias' => 'district_by_region'
                    , 'parent_field' => 'region', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))

                ->add('town', DependentFormsType::class, array('required' => false, 'entity_alias' => 'town_by_district'
                    , 'parent_field' => 'district', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))

//                ->add('town', 'sonata_type_model', array())
                
                ->add('locality', DependentFormsType::class, array('required' => false, 'entity_alias' => 'locality_by_town'
                    , 'parent_field' => 'town', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width:100%')), array('list' => 'list'))
                ->add('name')

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('createdAt')
                ->add('updatedAt')
                ->add('deletedAt')
        ;
    }

}
