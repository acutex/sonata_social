<?php

namespace Mfarm\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Mfarm\LocationBundle\Entity\Region;

class RegionAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('country', 'sonata_type_model', array('expanded' => false, 'multiple' => false))
                ->add('name')
        ;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
//                ->add('createdAt')
//                ->add('updatedAt')
                ->add('country')

        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('createdAt', 'doctrine_orm_date_range', array(), 'sonata_type_date_range_picker', array('field_options_start' => array('format' => 'yyyy-MM-dd'),
                    'field_options_end' => array('format' => 'yyyy-MM-dd'))
                )
                ->add('country', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))

        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('createdAt')
                ->add('updatedAt')
                ->add('country')

        ;
    }

    /**
     * Set region fields befor submitting data
     */
    public function prePersist($region) {
        foreach ($region->getDistricts() as $inputs) {
            $inputs->setRegion($region);
        }
    }

    /**
     * Set region fields befor submitting data
     */
    public function preUpdate($region) {
        foreach ($region->getDistricts() as $inputs) {
            $inputs->setRegion($region);
        }
    }

}
