<?php

namespace Mfarm\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Mfarm\LocationBundle\Entity\Country;

class CountryAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('name')
                ->add('countryCode', null, array('required' => true))
                ->add('smsPrice', null, array('required' => true))
                ->add('voicePrice', null, array('required' => true))
                ->add('language', 'choice', array('required' => true,
                    'choices' => array(
                        '' => 'Select Language',
                        'English' => 'en',
                        'French' => 'fr'
            )))
        ;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('countryCode', null, array('required' => true))
                ->add('smsPrice', null, array('required' => true))
                ->add('voicePrice', null, array('required' => true))
                ->add('language', null, array('required' => true))


        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('countryCode', null, array('required' => true))
                ->add('smsPrice', null, array('required' => true))
                ->add('voicePrice', null, array('required' => true))
                ->add('language', null, array('required' => true))

        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('countryCode', null, array('required' => true))
                ->add('smsPrice', null, array('required' => true))
                ->add('voicePrice', null, array('required' => true))
                ->add('language', null, array('required' => true))

        ;
    }

//    public function getTemplate($name) {
//        switch ($name) {
//            case 'list':
//                return 'LocationBundle:Admin:country_list.html.twig';
//                break;
//            default:
//                return parent::getTemplate($name);
//                break;
//        }
//    }

}
