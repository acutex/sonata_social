<?php

namespace Mfarm\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Mfarm\LocationBundle\Entity\Town;
use Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType;


class TownAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('country', 'sonata_type_model', array(), array('add' => 'list'))
                ->add('region', DependentFormsType::class, array('required' => false, 'entity_alias' => 'region_by_country'
                    , 'parent_field' => 'country', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
                ->add('district', DependentFormsType::class, array('required' => false, 'entity_alias' => 'district_by_region'
                    , 'parent_field' => 'region', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))

//                ->add('country', 'sonata_type_model', array('expanded' => false, 'multiple' => false))
//                ->add('region', 'sonata_type_model', array('expanded' => false, 'multiple' => false))
//                ->add('district', 'sonata_type_model', array('expanded' => false, 'multiple' => false))
                ->add('name')
        ;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('country')
                ->add('region')
                ->add('district')

        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('country', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))
                ->add('region', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))
                ->add('district', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))
                ->add('createdAt', 'doctrine_orm_date_range', array(), 'sonata_type_date_range_picker', array('field_options_start' => array('format' => 'yyyy-MM-dd'),
                    'field_options_end' => array('format' => 'yyyy-MM-dd'))
                )
        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('country')
                ->add('region')
                ->add('district')

        ;
    }

}
