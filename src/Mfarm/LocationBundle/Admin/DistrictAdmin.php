<?php

namespace Mfarm\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Mfarm\LocationBundle\Entity\District;
use Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType;


class DistrictAdmin extends AbstractAdmin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('country', 'sonata_type_model', array(), array('add' => 'list'))
                ->add('region', DependentFormsType::class, array('required' => false, 'entity_alias' => 'region_by_country'
                    , 'parent_field' => 'country', 'attr' => array('required' => false, 'class' => 'select2-offscreen','style'=>'width:100%;')), array('list' => 'list'))
//                ->add('province')
//                ->add('province', \Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType::class, array('required' => false, 'entity_alias' => 'province_by_region'
//                    , 'parent_field' => 'region', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
                ->add('name')
//                ->add('country', 'sonata_type_model', array('expanded' => false, 'multiple' => false))
//                ->add('region', 'sonata_type_model', array('expanded' => false, 'multiple' => false))

        ;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('country')
                ->add('region')
//                ->add('province')

        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('country', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))
                ->add('region', null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))
//                ->add('province')
                ->add('createdAt', 'doctrine_orm_date_range', array(), 'sonata_type_date_range_picker', array('field_options_start' => array('format' => 'yyyy-MM-dd'),
                    'field_options_end' => array('format' => 'yyyy-MM-dd'))
                )
        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('country')
                ->add('region')
//                ->add('province')

        ;
    }

    /**
     * Set district fields befor submitting data
     */
    public function prePersist($district) {
        foreach ($district->getTowns() as $inputs) {
            $inputs->setDistrict($district);
        }
    }

    /**
     * Set district fields befor submitting data
     */
    public function preUpdate($district) {
        foreach ($district->getTowns() as $inputs) {
            $inputs->setDistrict($district);
        }
    }

}
