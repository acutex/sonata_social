<?php

namespace Mfarm\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Mfarm\LocationBundle\Model\RegionInterface;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;

/**
 * Locality
 * 
 * @ORM\Table(name="locality")
 * @ORM\Entity(repositoryClass="Mfarm\LocationBundle\Repository\LocalityRepository")
 * @ExclusionPolicy("all")
 *
 */
class Locality {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * 
     * @Assert\Length(max= 100, maxMessage="region.error.name_maxlength")
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\LocationBundle\Entity\Country", inversedBy="locality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \Region
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="locality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id",nullable=true)
     * })
     * @GRID\Column(field="region.name", title="Region")
     */
    private $region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Province")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="province_id", referencedColumnName="id",nullable=true)
     * })
     *
     *
     */
    private $province;

    /**
     * @var \District
     *
     * @ORM\ManyToOne(targetEntity="District", inversedBy="locality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id",nullable=true)
     * })
     * @GRID\Column(field="district.name", title="District")
     */
    private $district;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Mfarm\LocationBundle\Entity\Town", inversedBy="locality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",nullable=true)
     * })
     * 

     */
    private $town;

    /**
     *
     * @ORM\OneToMany(targetEntity="Mfarm\LocationBundle\Entity\Village", mappedBy="locality", orphanRemoval=true, cascade={"persist"})
     */
    private $village;



    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString() {
        return $this->name."";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Region
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Region
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Region
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Region
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * @Expose
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     *
     * @Accessor(getter="getupdated")
     * @Expose
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     * @Expose
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }

    /**
     * Set town
     *
     * @param \Mfarm\LocationBundle\Entity\Town $town
     * @return Locality
     */
    public function setTown(\Mfarm\LocationBundle\Entity\Town $town = null) {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \Mfarm\LocationBundle\Entity\Town 
     */
    public function getTown() {
        return $this->town;
    }

    /**
     * Set country
     *
     * @param \Mfarm\LocationBundle\Entity\Country $country
     * @return Locality
     */
    public function setCountry(\Mfarm\LocationBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Mfarm\LocationBundle\Entity\Country 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return Locality
     */
    public function setRegion(\Mfarm\LocationBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Mfarm\LocationBundle\Entity\Region 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param \Mfarm\LocationBundle\Entity\District $district
     * @return Locality
     */
    public function setDistrict(\Mfarm\LocationBundle\Entity\District $district = null) {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \Mfarm\LocationBundle\Entity\District 
     */
    public function getDistrict() {
        return $this->district;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->village = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     * @return Locality
     */
    public function addVillage(\Mfarm\LocationBundle\Entity\Village $village) {
        $this->village[] = $village;

        return $this;
    }

    /**
     * Remove village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     */
    public function removeVillage(\Mfarm\LocationBundle\Entity\Village $village) {
        $this->village->removeElement($village);
    }

    /**
     * Get village
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVillage() {
        return $this->village;
    }

    /**
     * Set province
     *
     * @param \Mfarm\LocationBundle\Entity\Province $province
     * @return Locality
     */
    public function setProvince(\Mfarm\LocationBundle\Entity\Province $province = null) {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \Mfarm\LocationBundle\Entity\Province 
     */
    public function getProvince() {
        return $this->province;
    }

}
