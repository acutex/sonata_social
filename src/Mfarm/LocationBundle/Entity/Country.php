<?php

namespace Mfarm\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Mfarm\LocationBundle\Model\CountryInterface;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;

/**
 * Country
 * 
 * @ORM\Table (name="country")
 * @ORM\Entity(repositoryClass="Mfarm\LocationBundle\Repository\CountryRepository")
 * @ExclusionPolicy("all")
 *
 * @ExclusionPolicy("all")
 */
class Country implements CountryInterface {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "country.error.name_notblank", groups={"default_country"})
     * @Assert\Length(max= 100, maxMessage="country.error.name_maxlength", groups={"default_country"})
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=true)
     * @Expose
     */
    private $name;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\Region", mappedBy="country", orphanRemoval=true, cascade={"persist"})
     */
    private $region;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\Province", mappedBy="country", orphanRemoval=true, cascade={"persist"})
     */
    private $province;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\Locality", mappedBy="country", orphanRemoval=true, cascade={"persist"})
     */
    private $locality;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\Village", mappedBy="country", orphanRemoval=true, cascade={"persist"})
     */
    private $village;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Assert\NotBlank(message = "Please enter country code")
     * @ORM\Column(name="country_code", type="string", length=255, nullable=true, unique=true)
     *
     */
    private $countryCode;

    /**
     * @ORM\Column(name="sms_price", type="string", length=255, nullable=true)
     */
    private $smsPrice;

    /**
     * @ORM\Column(name="voice_price", type="string", length=255, nullable=true)
     */
    private $voicePrice;

    /**
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;



//    /**
//     *
//     * @ORM\OneToMany(targetEntity="\Mfarm\ComhubBundle\Entity\MsgScheduler", mappedBy="country")
//     */
//    private $msgScheduler;



    public function __toString() {
        return $this->name."";
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->region = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Add region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return Country
     */
    public function addRegion(\Mfarm\LocationBundle\Entity\Region $region) {
        $this->region[] = $region;

        return $this;
    }

    /**
     * Remove region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     */
    public function removeRegion(\Mfarm\LocationBundle\Entity\Region $region) {
        $this->region->removeElement($region);
    }

    /**
     * Get region
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Country
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Country
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getCountryCodeForUser() {
        return array(
            array('code' => "93", 'name' => "Afghanistan",),
            array('code' => "355", 'name' => "Albania",),
            array('code' => "213", 'name' => "Algeria",),
            array('code' => "1", 'name' => "American Samoa",),
            array('code' => "376", 'name' => "Andorra",),
            array('code' => "244", 'name' => "Angola",),
            array('code' => "1", 'name' => "Anguilla",),
            array('code' => "1", 'name' => "Antigua and Barbuda",),
            array('code' => "54", 'name' => "Argentina",),
            array('code' => "374", 'name' => "Armenia",),
            array('code' => "297", 'name' => "Aruba",),
            array('code' => "247", 'name' => "Ascension",),
            array('code' => "61", 'name' => "Australia",),
            array('code' => "43", 'name' => "Austria",),
            array('code' => "994", 'name' => "Azerbaijan",),
            array('code' => "1", 'name' => "Bahamas",),
            array('code' => "973", 'name' => "Bahrain",),
            array('code' => "880", 'name' => "Bangladesh",),
            array('code' => "1", 'name' => "Barbados",),
            array('code' => "375", 'name' => "Belarus",),
            array('code' => "32", 'name' => "Belgium",),
            array('code' => "501", 'name' => "Belize",),
            array('code' => "229", 'name' => "Benin",),
            array('code' => "1", 'name' => "Bermuda",),
            array('code' => "975", 'name' => "Bhutan",),
            array('code' => "591", 'name' => "Bolivia",),
            array('code' => "387", 'name' => "Bosnia and Herzegovina",),
            array('code' => "267", 'name' => "Botswana",),
            array('code' => "55", 'name' => "Brazil",),
            array('code' => "1", 'name' => "British Virgin Islands",),
            array('code' => "673", 'name' => "Brunei",),
            array('code' => "359", 'name' => "Bulgaria",),
            array('code' => "226", 'name' => "Burkina Faso",),
            array('code' => "257", 'name' => "Burundi",),
            array('code' => "855", 'name' => "Cambodia",),
            array('code' => "237", 'name' => "Cameroon",),
            array('code' => "1", 'name' => "Canada",),
            array('code' => "238", 'name' => "Cape Verde",),
            array('code' => "1", 'name' => "Cayman Islands",),
            array('code' => "236", 'name' => "Central African Republic",),
            array('code' => "235", 'name' => "Chad",),
            array('code' => "56", 'name' => "Chile",),
            array('code' => "86", 'name' => "China",),
            array('code' => "57", 'name' => "Colombia",),
            array('code' => "269", 'name' => "Comoros",),
            array('code' => "242", 'name' => "Congo",),
            array('code' => "682", 'name' => "Cook Islands",),
            array('code' => "506", 'name' => "Costa Rica",),
            array('code' => "385", 'name' => "Croatia",),
            array('code' => "53", 'name' => "Cuba",),
            array('code' => "357", 'name' => "Cyprus",),
            array('code' => "420", 'name' => "Czech Republic",),
            array('code' => "243", 'name' => "Democratic Republic of Congo",),
            array('code' => "45", 'name' => "Denmark",),
            array('code' => "246", 'name' => "Diego Garcia",),
            array('code' => "253", 'name' => "Djibouti",),
            array('code' => "1", 'name' => "Dominica",),
            array('code' => "1", 'name' => "Dominican Republic",),
            array('code' => "670", 'name' => "East Timor",),
            array('code' => "593", 'name' => "Ecuador",),
            array('code' => "20", 'name' => "Egypt",),
            array('code' => "503", 'name' => "El Salvador",),
            array('code' => "240", 'name' => "Equatorial Guinea",),
            array('code' => "291", 'name' => "Eritrea",),
            array('code' => "372", 'name' => "Estonia",),
            array('code' => "251", 'name' => "Ethiopia",),
            array('code' => "500", 'name' => "Falkland (Malvinas) Islands",),
            array('code' => "298", 'name' => "Faroe Islands",),
            array('code' => "679", 'name' => "Fiji",),
            array('code' => "358", 'name' => "Finland",),
            array('code' => "33", 'name' => "France",),
            array('code' => "594", 'name' => "French Guiana",),
            array('code' => "689", 'name' => "French Polynesia",),
            array('code' => "241", 'name' => "Gabon",),
            array('code' => "220", 'name' => "Gambia",),
            array('code' => "995", 'name' => "Georgia",),
            array('code' => "49", 'name' => "Germany",),
            array('code' => "233", 'name' => "Ghana",),
            array('code' => "350", 'name' => "Gibraltar",),
            array('code' => "30", 'name' => "Greece",),
            array('code' => "299", 'name' => "Greenland",),
            array('code' => "1", 'name' => "Grenada",),
            array('code' => "590", 'name' => "Guadeloupe",),
            array('code' => "1", 'name' => "Guam",),
            array('code' => "502", 'name' => "Guatemala",),
            array('code' => "224", 'name' => "Guinea",),
            array('code' => "245", 'name' => "Guinea-Bissau",),
            array('code' => "592", 'name' => "Guyana",),
            array('code' => "509", 'name' => "Haiti",),
            array('code' => "504", 'name' => "Honduras",),
            array('code' => "852", 'name' => "Hong Kong",),
            array('code' => "36", 'name' => "Hungary",),
            array('code' => "354", 'name' => "Iceland",),
            array('code' => "91", 'name' => "India",),
            array('code' => "62", 'name' => "Indonesia",),
            array('code' => "870", 'name' => "Inmarsat Satellite",),
            array('code' => "98", 'name' => "Iran",),
            array('code' => "964", 'name' => "Iraq",),
            array('code' => "353", 'name' => "Ireland",),
            array('code' => "972", 'name' => "Israel",),
            array('code' => "39", 'name' => "Italy",),
            array('code' => "225", 'name' => "Ivory Coast",),
            array('code' => "1", 'name' => "Jamaica",),
            array('code' => "81", 'name' => "Japan",),
            array('code' => "962", 'name' => "Jordan",),
            array('code' => "7", 'name' => "Kazakhstan",),
            array('code' => "254", 'name' => "Kenya",),
            array('code' => "686", 'name' => "Kiribati",),
            array('code' => "965", 'name' => "Kuwait",),
            array('code' => "996", 'name' => "Kyrgyzstan",),
            array('code' => "856", 'name' => "Laos",),
            array('code' => "371", 'name' => "Latvia",),
            array('code' => "961", 'name' => "Lebanon",),
            array('code' => "266", 'name' => "Lesotho",),
            array('code' => "231", 'name' => "Liberia",),
            array('code' => "218", 'name' => "Libya",),
            array('code' => "423", 'name' => "Liechtenstein",),
            array('code' => "370", 'name' => "Lithuania",),
            array('code' => "352", 'name' => "Luxembourg",),
            array('code' => "853", 'name' => "Macau",),
            array('code' => "389", 'name' => "Macedonia",),
            array('code' => "261", 'name' => "Madagascar",),
            array('code' => "265", 'name' => "Malawi",),
            array('code' => "60", 'name' => "Malaysia",),
            array('code' => "960", 'name' => "Maldives",),
            array('code' => "223", 'name' => "Mali",),
            array('code' => "356", 'name' => "Malta",),
            array('code' => "692", 'name' => "Marshall Islands",),
            array('code' => "596", 'name' => "Martinique",),
            array('code' => "222", 'name' => "Mauritania",),
            array('code' => "230", 'name' => "Mauritius",),
            array('code' => "262", 'name' => "Mayotte",),
            array('code' => "52", 'name' => "Mexico",),
            array('code' => "691", 'name' => "Micronesia",),
            array('code' => "373", 'name' => "Moldova",),
            array('code' => "377", 'name' => "Monaco",),
            array('code' => "976", 'name' => "Mongolia",),
            array('code' => "382", 'name' => "Montenegro",),
            array('code' => "1", 'name' => "Montserrat",),
            array('code' => "212", 'name' => "Morocco",),
            array('code' => "258", 'name' => "Mozambique",),
            array('code' => "95", 'name' => "Myanmar",),
            array('code' => "264", 'name' => "Namibia",),
            array('code' => "674", 'name' => "Nauru",),
            array('code' => "977", 'name' => "Nepal",),
            array('code' => "31", 'name' => "Netherlands",),
            array('code' => "599", 'name' => "Netherlands Antilles",),
            array('code' => "687", 'name' => "New Caledonia",),
            array('code' => "64", 'name' => "New Zealand",),
            array('code' => "505", 'name' => "Nicaragua",),
            array('code' => "227", 'name' => "Niger",),
            array('code' => "234", 'name' => "Nigeria",),
            array('code' => "683", 'name' => "Niue Island",),
            array('code' => "850", 'name' => "North Korea",),
            array('code' => "1", 'name' => "Northern Marianas",),
            array('code' => "47", 'name' => "Norway",),
            array('code' => "968", 'name' => "Oman",),
            array('code' => "92", 'name' => "Pakistan",),
            array('code' => "680", 'name' => "Palau",),
            array('code' => "507", 'name' => "Panama",),
            array('code' => "675", 'name' => "Papua New Guinea",),
            array('code' => "595", 'name' => "Paraguay",),
            array('code' => "51", 'name' => "Peru",),
            array('code' => "63", 'name' => "Philippines",),
            array('code' => "48", 'name' => "Poland",),
            array('code' => "351", 'name' => "Portugal",),
            array('code' => "1", 'name' => "Puerto Rico",),
            array('code' => "974", 'name' => "Qatar",),
            array('code' => "262", 'name' => "Reunion",),
            array('code' => "40", 'name' => "Romania",),
            array('code' => "7", 'name' => "Russian Federation",),
            array('code' => "250", 'name' => "Rwanda",),
            array('code' => "290", 'name' => "Saint Helena",),
            array('code' => "1", 'name' => "Saint Kitts and Nevis",),
            array('code' => "1", 'name' => "Saint Lucia",),
            array('code' => "508", 'name' => "Saint Pierre and Miquelon",),
            array('code' => "1", 'name' => "Saint Vincent and the Grenadines",),
            array('code' => "685", 'name' => "Samoa",),
            array('code' => "378", 'name' => "San Marino",),
            array('code' => "239", 'name' => "Sao Tome and Principe",),
            array('code' => "966", 'name' => "Saudi Arabia",),
            array('code' => "221", 'name' => "Senegal",),
            array('code' => "381", 'name' => "Serbia",),
            array('code' => "248", 'name' => "Seychelles",),
            array('code' => "232", 'name' => "Sierra Leone",),
            array('code' => "65", 'name' => "Singapore",),
            array('code' => "421", 'name' => "Slovakia",),
            array('code' => "386", 'name' => "Slovenia",),
            array('code' => "677", 'name' => "Solomon Islands",),
            array('code' => "252", 'name' => "Somalia",),
            array('code' => "27", 'name' => "South Africa",),
            array('code' => "82", 'name' => "South Korea",),
            array('code' => "34", 'name' => "Spain",),
            array('code' => "94", 'name' => "Sri Lanka",),
            array('code' => "249", 'name' => "Sudan",),
            array('code' => "597", 'name' => "Suriname",),
            array('code' => "268", 'name' => "Swaziland",),
            array('code' => "46", 'name' => "Sweden",),
            array('code' => "41", 'name' => "Switzerland",),
            array('code' => "963", 'name' => "Syria",),
            array('code' => "886", 'name' => "Taiwan",),
            array('code' => "992", 'name' => "Tajikistan",),
            array('code' => "255", 'name' => "Tanzania",),
            array('code' => "66", 'name' => "Thailand",),
            array('code' => "228", 'name' => "Togo",),
            array('code' => "690", 'name' => "Tokelau",),
            array('code' => "1", 'name' => "Trinidad and Tobago",),
            array('code' => "216", 'name' => "Tunisia",),
            array('code' => "90", 'name' => "Turkey",),
            array('code' => "993", 'name' => "Turkmenistan",),
            array('code' => "1", 'name' => "Turks and Caicos Islands",),
            array('code' => "688", 'name' => "Tuvalu",),
            array('code' => "256", 'name' => "Uganda",),
            array('code' => "380", 'name' => "Ukraine",),
            array('code' => "971", 'name' => "United Arab Emirates",),
            array('code' => "44", 'name' => "United Kingdom",),
            array('code' => "1", 'name' => "United States of America",),
            array('code' => "1", 'name' => "U.S. Virgin Islands",),
            array('code' => "598", 'name' => "Uruguay",),
            array('code' => "998", 'name' => "Uzbekistan",),
            array('code' => "678", 'name' => "Vanuatu",),
            array('code' => "379", 'name' => "Vatican City",),
            array('code' => "58", 'name' => "Venezuela",),
            array('code' => "84", 'name' => "Vietnam",),
            array('code' => "681", 'name' => "Wallis and Futuna",),
            array('code' => "967", 'name' => "Yemen",),
            array('code' => "260", 'name' => "Zambia",),
            array('code' => "263", 'name' => "Zimbabwe"),
        );
    }

    public function getCurrency() {
        return array(
            array('id' => "XUA", 'name' => "ADB Unit of Account"),
            array('id' => "AFN", 'name' => "Afghan Afghani"),
            array('id' => "AFA", 'name' => "Afghan Afghani (1927-2002)"),
            array('id' => "ALL", 'name' => "Albanian Lek"),
            array('id' => "ALK", 'name' => "Albanian Lek (1946-1965)"),
            array('id' => "DZD", 'name' => "Algerian Dinar"),
            array('id' => "ADP", 'name' => "Andorran Peseta"),
            array('id' => "AOA", 'name' => "Angolan Kwanza"),
            array('id' => "AOK", 'name' => "Angolan Kwanza (1977-1991)"),
            array('id' => "AON", 'name' => "Angolan New Kwanza (1990-2000)"),
            array('id' => "AOR", 'name' => "Angolan Readjusted Kwanza (1995-1999)"),
            array('id' => "ARA", 'name' => "Argentine Austral"),
            array('id' => "ARS", 'name' => "Argentine Peso"),
            array('id' => "ARM", 'name' => "Argentine Peso (1881-1970)"),
            array('id' => "ARP", 'name' => "Argentine Peso (1983-1985)"),
            array('id' => "ARL", 'name' => "Argentine Peso Ley (1970-1983)"),
            array('id' => "AMD", 'name' => "Armenian Dram"),
            array('id' => "AWG", 'name' => "Aruban Florin"),
            array('id' => "AUD", 'name' => "Australian Dollar"),
            array('id' => "ATS", 'name' => "Austrian Schilling"),
            array('id' => "AZN", 'name' => "Azerbaijani Manat"),
            array('id' => "AZM", 'name' => "Azerbaijani Manat (1993-2006)"),
            array('id' => "BSD", 'name' => "Bahamian Dollar"),
            array('id' => "BHD", 'name' => "Bahraini Dinar"),
            array('id' => "BDT", 'name' => "Bangladeshi Taka"),
            array('id' => "BBD", 'name' => "Barbadian Dollar"),
            array('id' => "BYB", 'name' => "Belarusian New Ruble (1994-1999)"),
            array('id' => "BYR", 'name' => "Belarusian Ruble"),
            array('id' => "BEF", 'name' => "Belgian Franc"),
            array('id' => "BEC", 'name' => "Belgian Franc (convertible)"),
            array('id' => "BEL", 'name' => "Belgian Franc (financial)"),
            array('id' => "BZD", 'name' => "Belize Dollar"),
            array('id' => "BMD", 'name' => "Bermudan Dollar"),
            array('id' => "BTN", 'name' => "Bhutanese Ngultrum"),
            array('id' => "BOB", 'name' => "Bolivian Boliviano"),
            array('id' => "BOL", 'name' => "Bolivian Boliviano (1863-1963)"),
            array('id' => "BOV", 'name' => "Bolivian Mvdol"),
            array('id' => "BOP", 'name' => "Bolivian Peso"),
            array('id' => "BAM", 'name' => "Bosnia-Herzegovina Convertible Mark"),
            array('id' => "BAD", 'name' => "Bosnia-Herzegovina Dinar (1992-1994)"),
            array('id' => "BAN", 'name' => "Bosnia-Herzegovina New Dinar (1994-1997)"),
            array('id' => "BWP", 'name' => "Botswanan Pula"),
            array('id' => "BRC", 'name' => "Brazilian Cruzado (1986-1989)"),
            array('id' => "BRZ", 'name' => "Brazilian Cruzeiro (1942-1967)"),
            array('id' => "BRE", 'name' => "Brazilian Cruzeiro (1990-1993)"),
            array('id' => "BRR", 'name' => "Brazilian Cruzeiro (1993-1994)"),
            array('id' => "BRN", 'name' => "Brazilian New Cruzado (1989-1990)"),
            array('id' => "BRB", 'name' => "Brazilian New Cruzeiro (1967-1986)"),
            array('id' => "BRL", 'name' => "Brazilian Real"),
            array('id' => "GBP", 'name' => "British Pound Sterling"),
            array('id' => "BND", 'name' => "Brunei Dollar"),
            array('id' => "BGL", 'name' => "Bulgarian Hard Lev"),
            array('id' => "BGN", 'name' => "Bulgarian Lev"),
            array('id' => "BGO", 'name' => "Bulgarian Lev (1879-1952)"),
            array('id' => "BGM", 'name' => "Bulgarian Socialist Lev"),
            array('id' => "BUK", 'name' => "Burmese Kyat"),
            array('id' => "BIF", 'name' => "Burundian Franc"),
            array('id' => "KHR", 'name' => "Cambodian Riel"),
            array('id' => "CAD", 'name' => "Canadian Dollar"),
            array('id' => "CVE", 'name' => "Cape Verdean Escudo"),
            array('id' => "KYD", 'name' => "Cayman Islands Dollar"),
            array('id' => "XOF", 'name' => "CFA Franc BCEAO"),
            array('id' => "XAF", 'name' => "CFA Franc BEAC"),
            array('id' => "XPF", 'name' => "CFP Franc"),
            array('id' => "CLE", 'name' => "Chilean Escudo"),
            array('id' => "CLP", 'name' => "Chilean Peso"),
            array('id' => "CLF", 'name' => "Chilean Unit of Account (UF)"),
            array('id' => "CNX", 'name' => "Chinese People’s Bank Dollar"),
            array('id' => "CNY", 'name' => "Chinese Yuan"),
            array('id' => "COP", 'name' => "Colombian Peso"),
            array('id' => "COU", 'name' => "Colombian Real Value Unit"),
            array('id' => "KMF", 'name' => "Comorian Franc"),
            array('id' => "CDF", 'name' => "Congolese Franc"),
            array('id' => "CRC", 'name' => "Costa Rican Colón"),
            array('id' => "HRD", 'name' => "Croatian Dinar"),
            array('id' => "HRK", 'name' => "Croatian Kuna"),
            array('id' => "CUC", 'name' => "Cuban Convertible Peso"),
            array('id' => "CUP", 'name' => "Cuban Peso"),
            array('id' => "CYP", 'name' => "Cypriot Pound"),
            array('id' => "CZK", 'name' => "Czech Republic Koruna"),
            array('id' => "CSK", 'name' => "Czechoslovak Hard Koruna"),
            array('id' => "DKK", 'name' => "Danish Krone"),
            array('id' => "DJF", 'name' => "Djiboutian Franc"),
            array('id' => "DOP", 'name' => "Dominican Peso"),
            array('id' => "NLG", 'name' => "Dutch Guilder"),
            array('id' => "XCD", 'name' => "East Caribbean Dollar"),
            array('id' => "DDM", 'name' => "East German Mark"),
            array('id' => "ECS", 'name' => "Ecuadorian Sucre"),
            array('id' => "ECV", 'name' => "Ecuadorian Unit of Constant Value"),
            array('id' => "EGP", 'name' => "Egyptian Pound"),
            array('id' => "GQE", 'name' => "Equatorial Guinean Ekwele"),
            array('id' => "ERN", 'name' => "Eritrean Nakfa"),
            array('id' => "EEK", 'name' => "Estonian Kroon"),
            array('id' => "ETB", 'name' => "Ethiopian Birr"),
            array('id' => "EUR", 'name' => "Euro"),
            array('id' => "XBA", 'name' => "European Composite Unit"),
            array('id' => "XEU", 'name' => "European Currency Unit"),
            array('id' => "XBB", 'name' => "European Monetary Unit"),
            array('id' => "XBC", 'name' => "European Unit of Account (XBC)"),
            array('id' => "XBD", 'name' => "European Unit of Account (XBD)"),
            array('id' => "FKP", 'name' => "Falkland Islands Pound"),
            array('id' => "FJD", 'name' => "Fijian Dollar"),
            array('id' => "FIM", 'name' => "Finnish Markka"),
            array('id' => "FRF", 'name' => "French Franc"),
            array('id' => "XFO", 'name' => "French Gold Franc"),
            array('id' => "XFU", 'name' => "French UIC-Franc"),
            array('id' => "GMD", 'name' => "Gambian Dalasi"),
            array('id' => "GEK", 'name' => "Georgian Kupon Larit"),
            array('id' => "GEL", 'name' => "Georgian Lari"),
            array('id' => "DEM", 'name' => "German Mark"),
            array('id' => "GHS", 'name' => "Ghanaian Cedi"),
            array('id' => "GHC", 'name' => "Ghanaian Cedi (1979-2007)"),
            array('id' => "GIP", 'name' => "Gibraltar Pound"),
            array('id' => "XAU", 'name' => "Gold"),
            array('id' => "GRD", 'name' => "Greek Drachma"),
            array('id' => "GTQ", 'name' => "Guatemalan Quetzal"),
            array('id' => "GWP", 'name' => "Guinea-Bissau Peso"),
            array('id' => "GNF", 'name' => "Guinean Franc"),
            array('id' => "GNS", 'name' => "Guinean Syli"),
            array('id' => "GYD", 'name' => "Guyanaese Dollar"),
            array('id' => "HTG", 'name' => "Haitian Gourde"),
            array('id' => "HNL", 'name' => "Honduran Lempira"),
            array('id' => "HKD", 'name' => "Hong Kong Dollar"),
            array('id' => "HUF", 'name' => "Hungarian Forint"),
            array('id' => "ISK", 'name' => "Icelandic Króna"),
            array('id' => "ISJ", 'name' => "Icelandic Króna (1918-1981)"),
            array('id' => "INR", 'name' => "Indian Rupee"),
            array('id' => "IDR", 'name' => "Indonesian Rupiah"),
            array('id' => "IRR", 'name' => "Iranian Rial"),
            array('id' => "IQD", 'name' => "Iraqi Dinar"),
            array('id' => "IEP", 'name' => "Irish Pound"),
            array('id' => "ILS", 'name' => "Israeli New Sheqel"),
            array('id' => "ILP", 'name' => "Israeli Pound"),
            array('id' => "ILR", 'name' => "Israeli Sheqel (1980-1985)"),
            array('id' => "ITL", 'name' => "Italian Lira"),
            array('id' => "JMD", 'name' => "Jamaican Dollar"),
            array('id' => "JPY", 'name' => "Japanese Yen"),
            array('id' => "JOD", 'name' => "Jordanian Dinar"),
            array('id' => "KZT", 'name' => "Kazakhstani Tenge"),
            array('id' => "KES", 'name' => "Kenyan Shilling"),
            array('id' => "KWD", 'name' => "Kuwaiti Dinar"),
            array('id' => "KGS", 'name' => "Kyrgystani Som"),
            array('id' => "LAK", 'name' => "Laotian Kip"),
            array('id' => "LVL", 'name' => "Latvian Lats"),
            array('id' => "LVR", 'name' => "Latvian Ruble"),
            array('id' => "LBP", 'name' => "Lebanese Pound"),
            array('id' => "LSL", 'name' => "Lesotho Loti"),
            array('id' => "LRD", 'name' => "Liberian Dollar"),
            array('id' => "LYD", 'name' => "Libyan Dinar"),
            array('id' => "LTL", 'name' => "Lithuanian Litas"),
            array('id' => "LTT", 'name' => "Lithuanian Talonas"),
            array('id' => "LUL", 'name' => "Luxembourg Financial Franc"),
            array('id' => "LUC", 'name' => "Luxembourgian Convertible Franc"),
            array('id' => "LUF", 'name' => "Luxembourgian Franc"),
            array('id' => "MOP", 'name' => "Macanese Pataca"),
            array('id' => "MKD", 'name' => "Macedonian Denar"),
            array('id' => "MKN", 'name' => "Macedonian Denar (1992-1993)"),
            array('id' => "MGA", 'name' => "Malagasy Ariary"),
            array('id' => "MGF", 'name' => "Malagasy Franc"),
            array('id' => "MWK", 'name' => "Malawian Kwacha"),
            array('id' => "MYR", 'name' => "Malaysian Ringgit"),
            array('id' => "MVR", 'name' => "Maldivian Rufiyaa"),
            array('id' => "MVP", 'name' => "Maldivian Rupee"),
            array('id' => "MLF", 'name' => "Malian Franc"),
            array('id' => "MTL", 'name' => "Maltese Lira"),
            array('id' => "MTP", 'name' => "Maltese Pound"),
            array('id' => "MRO", 'name' => "Mauritanian Ouguiya"),
            array('id' => "MUR", 'name' => "Mauritian Rupee"),
            array('id' => "MXV", 'name' => "Mexican Investment Unit"),
            array('id' => "MXN", 'name' => "Mexican Peso"),
            array('id' => "MXP", 'name' => "Mexican Silver Peso (1861-1992)"),
            array('id' => "MDC", 'name' => "Moldovan Cupon"),
            array('id' => "MDL", 'name' => "Moldovan Leu"),
            array('id' => "MCF", 'name' => "Monegasque Franc"),
            array('id' => "MNT", 'name' => "Mongolian Tugrik"),
            array('id' => "MAD", 'name' => "Moroccan Dirham"),
            array('id' => "MAF", 'name' => "Moroccan Franc"),
            array('id' => "MZE", 'name' => "Mozambican Escudo"),
            array('id' => "MZN", 'name' => "Mozambican Metical"),
            array('id' => "MZM", 'name' => "Mozambican Metical (1980-2006)"),
            array('id' => "MMK", 'name' => "Myanma Kyat"),
            array('id' => "NAD", 'name' => "Namibian Dollar"),
            array('id' => "NPR", 'name' => "Nepalese Rupee"),
            array('id' => "ANG", 'name' => "Netherlands Antillean Guilder"),
            array('id' => "TWD", 'name' => "New Taiwan Dollar"),
            array('id' => "NZD", 'name' => "New Zealand Dollar"),
            array('id' => "NIO", 'name' => "Nicaraguan Córdoba"),
            array('id' => "NIC", 'name' => "Nicaraguan Córdoba (1988-1991)"),
            array('id' => "NGN", 'name' => "Nigerian Naira"),
            array('id' => "KPW", 'name' => "North Korean Won"),
            array('id' => "NOK", 'name' => "Norwegian Krone"),
            array('id' => "OMR", 'name' => "Omani Rial"),
            array('id' => "PKR", 'name' => "Pakistani Rupee"),
            array('id' => "XPD", 'name' => "Palladium"),
            array('id' => "PAB", 'name' => "Panamanian Balboa"),
            array('id' => "PGK", 'name' => "Papua New Guinean Kina"),
            array('id' => "PYG", 'name' => "Paraguayan Guarani"),
            array('id' => "PEI", 'name' => "Peruvian Inti"),
            array('id' => "PEN", 'name' => "Peruvian Nuevo Sol"),
            array('id' => "PES", 'name' => "Peruvian Sol (1863-1965)"),
            array('id' => "PHP", 'name' => "Philippine Peso"),
            array('id' => "XPT", 'name' => "Platinum"),
            array('id' => "PLN", 'name' => "Polish Zloty"),
            array('id' => "PLZ", 'name' => "Polish Zloty (1950-1995)"),
            array('id' => "PTE", 'name' => "Portuguese Escudo"),
            array('id' => "GWE", 'name' => "Portuguese Guinea Escudo"),
            array('id' => "QAR", 'name' => "Qatari Rial"),
            array('id' => "RHD", 'name' => "Rhodesian Dollar"),
            array('id' => "XRE", 'name' => "RINET Funds"),
            array('id' => "RON", 'name' => "Romanian Leu"),
            array('id' => "ROL", 'name' => "Romanian Leu (1952-2006)"),
            array('id' => "RUB", 'name' => "Russian Ruble"),
            array('id' => "RUR", 'name' => "Russian Ruble (1991-1998)"),
            array('id' => "RWF", 'name' => "Rwandan Franc"),
            array('id' => "SHP", 'name' => "Saint Helena Pound"),
            array('id' => "SVC", 'name' => "Salvadoran Colón"),
            array('id' => "WST", 'name' => "Samoan Tala"),
            array('id' => "STD", 'name' => "São Tomé and Príncipe Dobra"),
            array('id' => "SAR", 'name' => "Saudi Riyal"),
            array('id' => "RSD", 'name' => "Serbian Dinar"),
            array('id' => "CSD", 'name' => "Serbian Dinar (2002-2006)"),
            array('id' => "SCR", 'name' => "Seychellois Rupee"),
            array('id' => "SLL", 'name' => "Sierra Leonean Leone"),
            array('id' => "XAG", 'name' => "Silver"),
            array('id' => "SGD", 'name' => "Singapore Dollar"),
            array('id' => "SKK", 'name' => "Slovak Koruna"),
            array('id' => "SIT", 'name' => "Slovenian Tolar"),
            array('id' => "SBD", 'name' => "Solomon Islands Dollar"),
            array('id' => "SOS", 'name' => "Somali Shilling"),
            array('id' => "ZAR", 'name' => "South African Rand"),
            array('id' => "ZAL", 'name' => "South African Rand (financial)"),
            array('id' => "KRH", 'name' => "South Korean Hwan (1953-1962)"),
            array('id' => "KRW", 'name' => "South Korean Won"),
            array('id' => "KRO", 'name' => "South Korean Won (1945-1953)"),
            array('id' => "SSP", 'name' => "South Sudanese Pound"),
            array('id' => "SUR", 'name' => "Soviet Rouble"),
            array('id' => "ESP", 'name' => "Spanish Peseta"),
            array('id' => "ESA", 'name' => "Spanish Peseta (A account)"),
            array('id' => "ESB", 'name' => "Spanish Peseta (convertible account)"),
            array('id' => "XDR", 'name' => "Special Drawing Rights"),
            array('id' => "LKR", 'name' => "Sri Lankan Rupee"),
            array('id' => "XSU", 'name' => "Sucre"),
            array('id' => "SDD", 'name' => "Sudanese Dinar (1992-2007)"),
            array('id' => "SDG", 'name' => "Sudanese Pound"),
            array('id' => "SDP", 'name' => "Sudanese Pound (1957-1998)"),
            array('id' => "SRD", 'name' => "Surinamese Dollar"),
            array('id' => "SRG", 'name' => "Surinamese Guilder"),
            array('id' => "SZL", 'name' => "Swazi Lilangeni"),
            array('id' => "SEK", 'name' => "Swedish Krona"),
            array('id' => "CHF", 'name' => "Swiss Franc"),
            array('id' => "SYP", 'name' => "Syrian Pound"),
            array('id' => "TJR", 'name' => "Tajikistani Ruble"),
            array('id' => "TJS", 'name' => "Tajikistani Somoni"),
            array('id' => "TZS", 'name' => "Tanzanian Shilling"),
            array('id' => "XTS", 'name' => "Testing Currency Code"),
            array('id' => "THB", 'name' => "Thai Baht"),
            array('id' => "TPE", 'name' => "Timorese Escudo"),
            array('id' => "TOP", 'name' => "Tongan Paʻanga"),
            array('id' => "TTD", 'name' => "Trinidad and Tobago Dollar"),
            array('id' => "TND", 'name' => "Tunisian Dinar"),
            array('id' => "TRY", 'name' => "Turkish Lira"),
            array('id' => "TRL", 'name' => "Turkish Lira (1922-2005)"),
            array('id' => "TMT", 'name' => "Turkmenistani Manat"),
            array('id' => "TMM", 'name' => "Turkmenistani Manat (1993-2009)"),
            array('id' => "UGX", 'name' => "Ugandan Shilling"),
            array('id' => "UGS", 'name' => "Ugandan Shilling (1966-1987)"),
            array('id' => "UAH", 'name' => "Ukrainian Hryvnia"),
            array('id' => "UAK", 'name' => "Ukrainian Karbovanets"),
            array('id' => "AED", 'name' => "United Arab Emirates Dirham"),
            array('id' => "XXX", 'name' => "Unknown Currency"),
            array('id' => "UYU", 'name' => "Uruguayan Peso"),
            array('id' => "UYP", 'name' => "Uruguayan Peso (1975-1993)"),
            array('id' => "UYI", 'name' => "Uruguayan Peso (Indexed Units)"),
            array('id' => "USD", 'name' => "US Dollar"),
            array('id' => "USN", 'name' => "US Dollar (Next day)"),
            array('id' => "USS", 'name' => "US Dollar (Same day)"),
            array('id' => "UZS", 'name' => "Uzbekistan Som"),
            array('id' => "VUV", 'name' => "Vanuatu Vatu"),
            array('id' => "VEF", 'name' => "Venezuelan Bolívar"),
            array('id' => "VEB", 'name' => "Venezuelan Bolívar (1871-2008)"),
            array('id' => "VND", 'name' => "Vietnamese Dong"),
            array('id' => "VNN", 'name' => "Vietnamese Dong (1978-1985)"),
            array('id' => "CHE", 'name' => "WIR Euro"),
            array('id' => "CHW", 'name' => "WIR Franc"),
            array('id' => "YDD", 'name' => "Yemeni Dinar"),
            array('id' => "YER", 'name' => "Yemeni Rial"),
            array('id' => "YUN", 'name' => "Yugoslavian Convertible Dinar (1990-1992)"),
            array('id' => "YUD", 'name' => "Yugoslavian Hard Dinar (1966-1990)"),
            array('id' => "YUM", 'name' => "Yugoslavian New Dinar (1994-2002)"),
            array('id' => "YUR", 'name' => "Yugoslavian Reformed Dinar (1992-1993)"),
            array('id' => "ZRN", 'name' => "Zairean New Zaire (1993-1998)"),
            array('id' => "ZRZ", 'name' => "Zairean Zaire (1971-1993)"),
            array('id' => "ZMW", 'name' => "Zambian Kwacha"),
            array('id' => "ZMK", 'name' => "Zambian Kwacha (1968-2012)"),
            array('id' => "ZWD", 'name' => "Zimbabwean Dollar (1980-2008)"),
            array('id' => "ZWR", 'name' => "Zimbabwean Dollar (2008)"),
            array('id' => "ZWL", 'name' => "Zimbabwean Dollar (2009)"),
        );
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Country
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Country
     */
    public function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Set smsPrice
     *
     * @param string $smsPrice
     * @return Country
     */
    public function setSmsPrice($smsPrice) {
        $this->smsPrice = $smsPrice;

        return $this;
    }

    /**
     * Get smsPrice
     *
     * @return string 
     */
    public function getSmsPrice() {
        return $this->smsPrice;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode() {
        return $this->countryCode;
    }

    /**
     * Set voicePrice
     *
     * @param string $voicePrice
     * @return Country
     */
    public function setVoicePrice($voicePrice) {
        $this->voicePrice = $voicePrice;

        return $this;
    }

    /**
     * Get voicePrice
     *
     * @return string 
     */
    public function getVoicePrice() {
        return $this->voicePrice;
    }

    /**
     *
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     * @Expose
     * @Accessor(getter="getupdated")
     *
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     *
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }










    /**
     * Add locality
     *
     * @param \Mfarm\LocationBundle\Entity\Locality $locality
     * @return Country
     */
    public function addLocality(\Mfarm\LocationBundle\Entity\Locality $locality) {
        $this->locality[] = $locality;

        return $this;
    }

    /**
     * Remove locality
     *
     * @param \Mfarm\LocationBundle\Entity\Locality $locality
     */
    public function removeLocality(\Mfarm\LocationBundle\Entity\Locality $locality) {
        $this->locality->removeElement($locality);
    }

    /**
     * Get locality
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocality() {
        return $this->locality;
    }

    /**
     * Add province
     *
     * @param \Mfarm\LocationBundle\Entity\Province $province
     * @return Country
     */
    public function addProvince(\Mfarm\LocationBundle\Entity\Province $province) {
        $this->province[] = $province;

        return $this;
    }

    /**
     * Remove province
     *
     * @param \Mfarm\LocationBundle\Entity\Province $province
     */
    public function removeProvince(\Mfarm\LocationBundle\Entity\Province $province) {
        $this->province->removeElement($province);
    }

    /**
     * Get province
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProvince() {
        return $this->province;
    }

    /**
     * Add village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     * @return Country
     */
    public function addVillage(\Mfarm\LocationBundle\Entity\Village $village) {
        $this->village[] = $village;

        return $this;
    }

    /**
     * Remove village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     */
    public function removeVillage(\Mfarm\LocationBundle\Entity\Village $village) {
        $this->village->removeElement($village);
    }

    /**
     * Get village
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVillage() {
        return $this->village;
    }


    /**
     * Set language
     *
     * @param string $language
     * @return Country
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }


}
