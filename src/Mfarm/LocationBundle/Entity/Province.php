<?php

namespace Mfarm\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Mfarm\LocationBundle\Model\RegionInterface;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;

/**
 * Region
 * 
 * @ORM\Table(name="province")
 * @ORM\Entity(repositoryClass="Mfarm\LocationBundle\Repository\ProvinceRepository")
 * @ExclusionPolicy("all")
 *
 */
class Province {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * 
     * @Assert\Length(max= 100, maxMessage="region.error.name_maxlength")
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;
    
    
        /**
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="province")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id",nullable=true)
     * })
     * @GRID\Column(field="country.name", title="Country")

     */
    private $country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Mfarm\LocationBundle\Entity\Region", inversedBy="province")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     * 

     */
    private $region;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\District", mappedBy="province", orphanRemoval=true, cascade={"persist"})
     */
    private $districts;

//    /**
//     * @var \ClusterContribution
//     *
//     * @ORM\OneToMany(targetEntity="\Mfarm\LoanBundle\Entity\LoanCredit", mappedBy="province")
//     *
//     */
//    private $loanCredit;
    
    
        



    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString() {
        return $this->name."";
    }

    /**
     * @Expose
     * @Accessor(getter="getRegionId",setter="setRegion")
     */
    private $regionId;

    /**
     * Get countryid
     *
     */
    public function getRegionId() {
        return $this->region ? $this->region->getId() : null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Region
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Region
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Region
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Region
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * @Expose
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     *
     * @Accessor(getter="getupdated")
     * @Expose
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     * @Expose
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }

    /**
     * Set region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return Province
     */
    public function setRegion(\Mfarm\LocationBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Mfarm\LocationBundle\Entity\Region 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->districts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add districts
     *
     * @param \Mfarm\LocationBundle\Entity\District $districts
     * @return Province
     */
    public function addDistrict(\Mfarm\LocationBundle\Entity\District $districts) {
        $this->districts[] = $districts;

        return $this;
    }

    /**
     * Remove districts
     *
     * @param \Mfarm\LocationBundle\Entity\District $districts
     */
    public function removeDistrict(\Mfarm\LocationBundle\Entity\District $districts) {
        $this->districts->removeElement($districts);
    }

    /**
     * Get districts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDistricts() {
        return $this->districts;
    }

//    /**
//     * Add loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     * @return Province
//     */
//    public function addLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit[] = $loanCredit;
//
//        return $this;
//    }
//
//    /**
//     * Remove loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     */
//    public function removeLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit->removeElement($loanCredit);
//    }
//
//    /**
//     * Get loanCredit
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getLoanCredit() {
//        return $this->loanCredit;
//    }


    /**
     * Set country
     *
     * @param \Mfarm\LocationBundle\Entity\Country $country
     * @return Province
     */
    public function setCountry(\Mfarm\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Mfarm\LocationBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
