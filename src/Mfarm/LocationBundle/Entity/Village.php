<?php

namespace Mfarm\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Mfarm\LocationBundle\Model\TownInterface;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;


/**
 * Town
 *
 * @ORM\Table(name="village")
 * @ORM\Entity(repositoryClass="Mfarm\LocationBundle\Repository\TownRepository")
 * @ExclusionPolicy("all")
 */
class Village {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @var string
     * 
     * @Assert\Length(max= 100, maxMessage="town.error.name_maxlength", groups={"default_town"})
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;
    
    
    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="\Mfarm\LocationBundle\Entity\Country", inversedBy="village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \Region
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     * @GRID\Column(field="region.name", title="Region")
     */
    private $region;

    /**
     * @var \District
     *
     * @ORM\ManyToOne(targetEntity="District", inversedBy="village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id")
     * })
     * @GRID\Column(field="district.name", title="District")
     */
    private $district;

    /**
     * @var \District
     *
     * @ORM\ManyToOne(targetEntity="Town", inversedBy="village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",nullable=true)
     * })
     * @GRID\Column(field="town.name", title="Town")
     */
    private $town;

    /**
     * @var \District
     *
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locality_id", referencedColumnName="id",nullable=true)
     * })
     * 
     */
    private $locality;

//    /**
//     * @var \ClusterContribution
//     *
//     * @ORM\OneToMany(targetEntity="\Mfarm\LoanBundle\Entity\LoanCredit", mappedBy="village")
//     *
//     */
//    private $loanCredit;
    
    


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString() {
        return $this->name."";
    }

    /**
     * @Expose
     * @Accessor(getter="getTownId",setter="setTown")
     * @SerializedName("town")
     */
    private $townId;

    /**
     * Get countryid
     *
     */
    public function getTownId() {
        return $this->town ? $this->town->getId() : null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     * @Expose
     * @Accessor(getter="getupdated")
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Village
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Village
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Village
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Village
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set town
     *
     * @param \Mfarm\LocationBundle\Entity\Town $town
     * @return Village
     */
    public function setTown(\Mfarm\LocationBundle\Entity\Town $town = null) {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \Mfarm\LocationBundle\Entity\Town 
     */
    public function getTown() {
        return $this->town;
    }

    /**
     * Constructor
     */
    public function __construct() {
//        $this->loanCredit = new \Doctrine\Common\Collections\ArrayCollection();
    }

//    /**
//     * Add loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     * @return Village
//     */
//    public function addLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit[] = $loanCredit;
//
//        return $this;
//    }
//
//    /**
//     * Remove loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     */
//    public function removeLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit->removeElement($loanCredit);
//    }
//
//    /**
//     * Get loanCredit
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getLoanCredit() {
//        return $this->loanCredit;
//    }


    /**
     * Set locality
     *
     * @param \Mfarm\LocationBundle\Entity\Locality $locality
     * @return Village
     */
    public function setLocality(\Mfarm\LocationBundle\Entity\Locality $locality = null)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return \Mfarm\LocationBundle\Entity\Locality 
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set country
     *
     * @param \Mfarm\LocationBundle\Entity\Country $country
     * @return Village
     */
    public function setCountry(\Mfarm\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Mfarm\LocationBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return Village
     */
    public function setRegion(\Mfarm\LocationBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Mfarm\LocationBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param \Mfarm\LocationBundle\Entity\District $district
     * @return Village
     */
    public function setDistrict(\Mfarm\LocationBundle\Entity\District $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \Mfarm\LocationBundle\Entity\District 
     */
    public function getDistrict()
    {
        return $this->district;
    }
}
