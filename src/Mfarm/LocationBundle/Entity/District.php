<?php

namespace Mfarm\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Mfarm\LocationBundle\Model\DistrictInterface;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\SerializedName;


/**
 * District
 *
 * @ORM\Table(name="district")
 * @ORM\Entity(repositoryClass="Mfarm\LocationBundle\Repository\DistrictRepository")
 * @ExclusionPolicy("all")
 *
 */
class District implements DistrictInterface {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "district.error.name_notblank", groups={"default_district"})
     * @Assert\Length(max= 100, maxMessage="district.error.name_maxlength", groups={"default_district"})
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @GRID\Column(field="country.name", title="Country")
     */
    private $country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="districts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     * @GRID\Column(field="region.name", title="Region")
     * 
     */
    private $region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="districts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="province_id", referencedColumnName="id")
     * })
     * 
     * 
     */
    private $province;

    /**
     *
     * @ORM\OneToMany(targetEntity="Mfarm\LocationBundle\Entity\Town", mappedBy="district", orphanRemoval=true, cascade={"persist"})
     */
    private $towns;
    
    
    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\LocationBundle\Entity\Village", mappedBy="district", orphanRemoval=true, cascade={"persist"})
     */
    private $village;
    /**
     *
     * @ORM\OneToMany(targetEntity="Mfarm\LocationBundle\Entity\Locality", mappedBy="district", orphanRemoval=true, cascade={"persist"})
     */
    private $locality;

            

    
//    /**
//     * @var \ClusterContribution
//     *
//     * @ORM\OneToMany(targetEntity="\Mfarm\LoanBundle\Entity\LoanCredit", mappedBy="district")
//     *
//     */
//    private $loanCredit;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString() {
        return $this->name."";
    }

    /**
     * @Expose
     * @Accessor(getter="getRegionId",setter="setRegion")
     * @SerializedName("region")
     */
    private $regionId;

    /**
     * Get countryid
     *
     */
    public function getRegionId() {
        return $this->region ? $this->region->getId() : null;
    }

    /**
     * @Accessor(getter="getProvinceId",setter="setProvince")
     * @SerializedName("province")
     */
    private $provinceId;

    /**
     * Get countryid
     *
     */
    public function getProvinceId() {
        return $this->province ? $this->province->getId() : null;
    }

    /**
     * @Expose
     * @Accessor(getter="getCountryId",setter="setCountry")
     * @SerializedName("country")
     */
    private $countryId;

    /**
     * Get countryid
     *
     */
    public function getCountryId() {
        return $this->country ? $this->country->getId() : null;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->towns = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return District
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param \Mfarm\LocationBundle\Entity\Country $country
     * @return District
     */
    public function setCountry(\Mfarm\LocationBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Mfarm\LocationBundle\Entity\Country 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return District
     */
    public function setRegion(\Mfarm\LocationBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Mfarm\LocationBundle\Entity\Region 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Add towns
     *
     * @param \Mfarm\LocationBundle\Entity\Town $towns
     * @return District
     */
    public function addTown(\Mfarm\LocationBundle\Entity\Town $towns) {
        $this->towns[] = $towns;

        return $this;
    }

    /**
     * Remove towns
     *
     * @param \Mfarm\LocationBundle\Entity\Town $towns
     */
    public function removeTown(\Mfarm\LocationBundle\Entity\Town $towns) {
        $this->towns->removeElement($towns);
    }

    /**
     * Get towns
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTowns() {
        return $this->towns;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return District
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return District
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return District
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     * @Expose
     * @Accessor(getter="getupdated")
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }

    /**
     * Set province
     *
     * @param \Mfarm\LocationBundle\Entity\Province $province
     * @return District
     */
    public function setProvince(\Mfarm\LocationBundle\Entity\Province $province = null) {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \Mfarm\LocationBundle\Entity\Province 
     */
    public function getProvince() {
        return $this->province;
    }


//    /**
//     * Add loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     * @return District
//     */
//    public function addLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit)
//    {
//        $this->loanCredit[] = $loanCredit;
//
//        return $this;
//    }
//
//    /**
//     * Remove loanCredit
//     *
//     * @param \Mfarm\LoanBundle\Entity\LoanCredit $loanCredit
//     */
//    public function removeLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit)
//    {
//        $this->loanCredit->removeElement($loanCredit);
//    }
//
//    /**
//     * Get loanCredit
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getLoanCredit()
//    {
//        return $this->loanCredit;
//    }

    /**
     * Add locality
     *
     * @param \Mfarm\LocationBundle\Entity\Locality $locality
     * @return District
     */
    public function addLocality(\Mfarm\LocationBundle\Entity\Locality $locality)
    {
        $this->locality[] = $locality;

        return $this;
    }

    /**
     * Remove locality
     *
     * @param \Mfarm\LocationBundle\Entity\Locality $locality
     */
    public function removeLocality(\Mfarm\LocationBundle\Entity\Locality $locality)
    {
        $this->locality->removeElement($locality);
    }

    /**
     * Get locality
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Add village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     * @return District
     */
    public function addVillage(\Mfarm\LocationBundle\Entity\Village $village)
    {
        $this->village[] = $village;

        return $this;
    }

    /**
     * Remove village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     */
    public function removeVillage(\Mfarm\LocationBundle\Entity\Village $village)
    {
        $this->village->removeElement($village);
    }

    /**
     * Get village
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVillage()
    {
        return $this->village;
    }




}
