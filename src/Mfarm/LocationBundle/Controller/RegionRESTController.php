<?php

namespace Mfarm\LocationBundle\Controller;

use Mfarm\LocationBundle\Entity\Region;
use Mfarm\LocationBundle\Form\RegionType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Voryx\RESTGeneratorBundle\Controller\VoryxController;
use Mfarm\ActorBundle\Handler\ApiConfiguration;


/**
 * Region controller.
 * @RouteResource("Region")
 */
class RegionRESTController extends VoryxController {

    /**
     * Get a Region entity
     * @ApiDoc(
     *  resource=true,
     *  description="Region based on id",
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Region $entity) {
        return $entity;
    }

    /**
     * Get all Region entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, map=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, map=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
//        dump($this->get('request_stack')->getCurrentRequest('headers'));
//        exit;
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            unset($_GET['user']);
            $queryBuilder = $em->getRepository('LocationBundle:Region')->createQueryBuilder('farmer');
            $entities = ApiConfiguration::filterEntities($queryBuilder);
            return $entities;
            
            
            return FOSView::create('Not Found', Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Create an Country entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},

     *   }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction(Request $request) {
        $entity = new Region();
        $form = $this->createForm(new RegionType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return array('data' => $entity, 'status' => true);
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Update Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, Region $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new RegionType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return array('data' => $entity, 'status' => true);
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Partial Update to a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
     * {"name"="name", "dataType"="string", "required"=true, "description"="name"},
     * {"name"="country", "dataType"="integer", "required"=true, "description"="country id "},
     *   }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, Region $entity) {
        return $this->putAction($request, $entity);
    }

    /**
     * Delete a Region entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Partial Update an Region entity",
     *  parameters={
      }
     * )
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function deleteAction(Request $request, Region $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return array('status'=>true,'data'=>  sprintf('entity with id %d removed', $entity->getId()));;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
