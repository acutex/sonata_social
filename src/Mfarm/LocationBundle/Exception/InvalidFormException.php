<?php

namespace Mfarm\LocationBundle\Exception;



class InvalidFormException extends \RuntimeException {

  protected $form;
  protected $entity;

  public function __construct($message, $form = null, $entity = null)
  {
      parent::__construct($message);
      $this->form = $form;
      $this->entity = $entity;
  }

  /**
   * @return array|null
   */
  public function getForm()
  {
      if($this->entity)
        return array('form'=>$this->form,'entity'=>$this->entity);
      else
        return array('form'=>$this->form);
  }
}
