<?php

namespace Mfarm\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Mfarm\UserBundle\Model\UserInterface;
use Mfarm\UserBundle\Form\UserType;
use Mfarm\UserBundle\Exception\InvalidFormException;

/**
 * User Handler 
 * @DI\Service("userbundle.user.handler")
 */
class UserHandler implements UserHandlerInterface {

    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;

    /**
     * @DI\InjectParams({
     *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
     *     "formFactory" = @DI\Inject("form.factory"),
     *     "securityContext" = @DI\Inject("security.token_storage"),
     * })
     */
    public function __construct(ObjectManager $om, FormFactoryInterface $formFactory, $securityContext) {
        $this->om = $om;
        $this->entityClass = "Mfarm\UserBundle\Entity\User";
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
        $this->context = $securityContext;
    }

    /**
     * Get a User.
     *
     * @param mixed $id
     *
     * @return UserInterface
     */
    public function get($id) {
        return $this->repository->find($id);
    }

    /**
     * Get a list of Users.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0) {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    /**
     * Get a list of Users.
     *
     * @return array
     */
    public function allDataQuery() {
        return $this->repository->getAll();
    }

    /**
     * Create a new User.
     *
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function post(array $parameters) {
        $user = $this->createUser();

        return $this->processForm($user, $parameters, 'POST');
    }

    /**
     * Edit a User.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function put(UserInterface $user, array $parameters) {
        return $this->processForm($user, $parameters, 'PUT');
    }

    /**
     * Partially update a User.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function patch(UserInterface $user, array $parameters) {
        return $this->processForm($user, $parameters, 'PATCH');
    }

    /**
     * Delete a User.
     *
     * @return boolean
     */
    public function delete($id) {
        $entity = $this->repository->find($id);
        if (is_object($entity)) {
            $this->om->remove($entity);
            $this->om->flush();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Processes the form.
     *
     * @param UserInterface $user
     * @param array         $parameters
     * @param String        $method
     *
     * @return UserInterface
     *
     * @throws Mfarm\UserBundle\Exception\InvalidFormException
     */
    private function processForm(UserInterface $user, array $parameters, $method = "PUT") {
        $logo = $user->getLogo();
        $form = $this->formFactory->create(new UserType($this->context), $user, array('method' => $method));
        if (array_key_exists('_method', $parameters))
            unset($parameters['_method']);
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $user = $form->getData();
            $user->setUserType($this->getAggregatorType());
            $user->setEnabled(true);

            if ($user->getLogo() == null) {
                if ($logo) {
                    $user->setLogo($logo);
                }
            } else {
                if ($logo) {
                    $user->upload($logo);
                } else {
                    $user->upload();
                }
            }

            $this->om->persist($user);
            $this->om->flush($user);
            return $user;
        }
        throw new InvalidFormException('Invalid submitted data', $form, $user);
    }

    private function createUser() {
        return new $this->entityClass();
    }

    public function addUserGroup($groupObject, $userId) {
        if ($groupObject && $userId) {
            foreach ($userId as $value) {
                $entity = $this->repository->find($value);

                if (is_object($entity)) {
                    $entity->setUserGroup($groupObject);
                    $this->om->persist($entity);
                }
            }
            $this->om->flush();
            return true;
        } else {
            return false;
        }
    }

    public function deleteUserGroup($userId) {
        if ($userId) {
            $entity = $this->repository->find($userId);

      if(is_object($entity)){
                $entity->setUserGroup(null);
                $this->om->persist($entity);
            }
            $this->om->flush();
            return true;
    }else{
            return false;
        }
    }

  public function getAggregatorType(){
        return 'aggregator';
    }

  public function getAgentType(){
        return 'agent';
    }
}
