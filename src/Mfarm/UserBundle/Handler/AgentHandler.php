<?php

namespace Mfarm\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Mfarm\UserBundle\Model\UserInterface;
use Mfarm\UserBundle\Model\AgentInterface;
use Mfarm\UserBundle\Form\AgentType;
use Mfarm\UserBundle\Exception\InvalidFormException;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Agent Handler 
 * @DI\Service("userbundle.agent.handler")
 */
      
class AgentHandler implements AgentHandlerInterface {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;
  private $context;
  private $container;
  private $userManager;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   *     "securityContext" = @DI\Inject("security.token_storage", required = false),
   *     "container" = @DI\Inject("service_container", required = false),
   *     "userManager" = @DI\Inject("user.manager", required = false)
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory, $securityContext, ContainerInterface $container = null, $userManager)
  {
      $this->om = $om;
      $this->entityClass = "Mfarm\UserBundle\Entity\User";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
      $this->context = $securityContext;
      $this->container = $container;
      $this->userManager = $userManager;
  }

  /**
   * Get a Agent.
   *
   * @param mixed $id
   *
   * @return AgentInterface
   */
  public function get(UserInterface $parent, $id)
  {
      $entity = $this->repository->find($id);
      if(is_object($entity) && is_object($parent) && $entity->getParent() && $entity->getParent()->getId() == $parent->getId() )
      {
        return $this->repository->find($id);
      }
      else{
        return false;
      }        
  }

  /**
   * Get a list of Agent.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all(UserInterface $parent, $limit = 5, $offset = 0)
  {
      return $this->repository->findByFarmer($parent, null, $limit, $offset);

  }

  /**
   * Get a list of Agent.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function allAggentByUser($parent,$limit = 5, $offset = 0)
  {
      return $this->repository->findByParent($parent);

  }

  /**
   * Create a new Agent.
   *
   * @param array $parameters
   *
   * @return AgentInterface
   */
  public function post(array $parameters, UserInterface $parent)
  {
      $agent = $this->createAgent($parent);
      return $this->processForm($agent, $parameters, 'POST');
  }

  /**
   * Edit a Agent.
   *
   * @param AgentInterface $agent
   * @param array         $parameters
   *
   * @return AgentInterface
   */
  public function put(AgentInterface $agent, array $parameters)
  {
    return $this->processForm($agent, $parameters, 'PUT');
  }

  /**
   * Partially update a Agent.
   *
   * @param AgentInterface $agent
   * @param array         $parameters
   *
   * @return AgentInterface
   */
  public function patch(AgentInterface $agent, array $parameters)
  {
      return $this->processForm($agent, $parameters, 'PATCH');
  }

  /**
   * Delete Agent.
   *
   * @return boolean
   */
  public function delete($id){
      $entity = $this->repository->find($id);
      if(is_object($entity)){
        $entity->setDeletedAt( new \DateTime('now') );
        foreach ($entity->getFarmerGroups() as $key => $value) {
            $value->setAgent(null);
            $this->om->persist($value);
        }
        $this->om->persist($entity);
        $this->om->flush();
        return true;
      }else{
        return false;
      }

  }

  /**
   * Processes the form.
   *
   * @param AgentInterface $agent
   * @param array         $parameters
   * @param String        $method
   *
   * @return AgentInterface
   *
   * @throws Mfarm\UserBundle\Exception\InvalidFormException
   */
  private function processForm(AgentInterface $agent, array $parameters, $method = "PUT")
  {
      $currentPassword = $agent->getPassword();
      $logo = $agent->getLogo();
      $form = $this->formFactory->create(new AgentType(), $agent, array('method' => $method));
      if(array_key_exists('_method', $parameters))
        unset($parameters['_method']);
      $form->submit($parameters, 'PATCH' !== $method);
      $factory = $this->container->get('security.encoder_factory');
      $user = $this->context->getToken()->getUser();
      if(!is_object($user)){
        throw new InvalidFormException('Invalid submitted data', $form, $agent);
      }
      if ($form->isValid()) {

          $agent = $form->getData();
          if($agent->getPassword()){
              $encoder = $factory->getEncoder($agent);
              $password = $encoder->encodePassword($agent->getPassword(), $agent->getSalt());
              $agent->setPassword($password);
          }else{
              $agent->setPassword($currentPassword);
          }
          if($agent->getLogo() == null){
            if($logo){
              $agent->setLogo($logo);
            }
          }else
          {
            if($logo){
              $agent->upload($logo);
            }else{
              $agent->upload();
            }
          }
          $agent->setEnabled(true);
          $agent->setUserType($this->container->get('userbundle.user.handler')->getAgentType());
          if(!$agent->getParent()){
            $agent->setParent($user);
          }
          $roles = array('ROLE_AGENT');
          $agent->setRoles($roles);
          $this->om->persist($agent);
          $this->om->flush($agent);

          return $agent;
      }

      throw new InvalidFormException('Invalid submitted data', $form, $agent);
  }

  private function createAgent($parent = null)
  {
      $entity = new $this->entityClass();

      if ($parent){

          $entity->setParent($parent);
      }
      return $entity;
  }

  /**
   * Get a list of Agents.
   *
   * @return array
   */
  public function allDataQuery($parentId)
  {
      return $this->repository->getAll($parentId);
  }

  /**
   * Get a list of Agents.
   *
   * @return array
   */
  public function countAllDataQuery($parentId)
  {
      return $this->repository->countAll($parentId);
  }
  
  public function isSafeToDetele($id){
    $agent = $this->repository->find($id);
    $ex = $this->om->getRepository('ExtensionBundle:ExtensionAgentUpdate')->findOneByAgent($agent);
    if($ex){
      return false;
    }else{
      return true;
    }

  }  

  public function getMapData($options){
    $user = $this->userManager->getCurrentUser();
    switch ($options['userType']) {
      case 'agent':
        return $this->om->getRepository('UserBundle:User')->findByOptions($user, $options);
        break;

      case 'aggregator':
        return $this->om->getRepository('UserBundle:User')->findByOptions($user, $options);
        break;
      case 'farmer':
        return $this->om->getRepository('FarmerBundle:Farmer')->findByOptions($user, $options);
        break;
      case 'fbo':
        return $this->om->getRepository('AssociationBundle:Association')->findByOptions($user, $options);
        break;
      default:
        break;
    }
  }
  public function getDataForSync($user){
    if(array_key_exists('softdeleteable',$this->om->getFilters()->getEnabledFilters())){
        $this->om->getFilters()->disable('softdeleteable');
    }
    return$this->repository->getDataForSync($user);
  }
}
