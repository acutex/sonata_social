<?php


namespace Mfarm\UserBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Mfarm\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class FacebookAuthenticator extends SocialAuthenticator
{
    private $clientRegistry;
    private $em;
    private $router;
    private $tokenStorage;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router,
        TokenStorageInterface $tokenStorage)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    public function supports(Request $request)
    {
dump('1');
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    public function getCredentials(Request $request)
    {
        dump('2');
        // this method is only called if supports() returns true

        // For Symfony lower than 3.4 the supports method need to be called manually here:
        // if (!$this->supports($request)) {
        //     return null;
        // }
//        dump($this->getFacebookClient());
//        dump($this->fetchAccessToken($this->getFacebookClient()));exit();
        return $this->fetchAccessToken($this->getFacebookClient());
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        dump('3');

        /** @var FacebookUser $facebookUser */
        $facebookUser = $this->getFacebookClient()
            ->fetchUserFromToken($credentials);

        $email = $facebookUser->getEmail();

        // 1) have they logged in with Facebook before? Easy!
        $existingUser = $this->em->getRepository(User::class)
            ->findOneBy(['facebook_id' => $facebookUser->getId()]);
        if ($existingUser) {
            return $existingUser;
        }

        // 2) do we have a matching user by email?
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['email' => $email]);

        if (null === $user) {
            $user = new User();
            $user->setName($facebookUser->getName());
            $user->setFirstname($facebookUser->getFirstName());
            $user->setLastname($facebookUser->getLastName());
            $user->setEmail($facebookUser->getEmail());
            $user->setEmailCanonical($facebookUser->getEmail());
            $user->setEnabled(true);
            $user->setPix($facebookUser->getPictureUrl());
            $user->setFacebookId($facebookUser->getId());
            $user->setUsername($facebookUser->getEmail());
            $user->setUsernameCanonical($facebookUser->getEmail());
            $user->setPassword($facebookUser->getId());
            $user->setPlainpassword($facebookUser->getId());
            $user->setUserType('aggregator');
            $user->setRoles(['ROLE_ADMIN', 'ROLE_SONATA_ADMIN', '']);
            $this->em->persist($user);
            $this->em->flush();
        }

        return $user;
    }

    /**
     * @return FacebookClient
     */
    private function getFacebookClient()
    {
dump('4');
        return $this->clientRegistry
            ->getClient('facebook_main');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        dump('5');
        dump($token->getUser());
        dump($providerKey);
//        exit('authent');

        // change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('sonata_admin_dashboard');

        return new RedirectResponse($targetUrl);

        // or, on success, let the request continue to be handled by the controller
        //return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        dump('6');
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $token = $this->tokenStorage->getToken();
        dump($token);
        dump('out');
//        exit();
        return new RedirectResponse(
            '/admin/login', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}
