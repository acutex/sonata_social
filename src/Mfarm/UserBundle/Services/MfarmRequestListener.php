<?php 
namespace Mfarm\UserBundle\Services;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Sms Handler 
 * @DI\Service("security.manager")
 * @DI\Tag("kernel.event_listener", attributes = {"event" = "kernel.request", "method"="onKernelRequest"})
 */
class MfarmRequestListener
{

	private $om;
	private $auth;
	private $security;

	/**
	* @DI\InjectParams({
	*     "om" = @DI\Inject("doctrine.orm.entity_manager"),
    *     "auth" = @DI\Inject("security.authorization_checker", required = false),
    *     "security" = @DI\Inject("security.token_storage", required = false),
	*     "container" = @DI\Inject("service_container", required = false),
	* })
	*/
	public function __construct(ObjectManager $om, $auth, $security, $container)
	{
		$this->om = $om;
        $this->auth = $auth;
        $this->security = $security;
		$this->container = $container;
	}
    public function onKernelRequest(GetResponseEvent $event)
    {

//        dump($this->auth->isGranted('ROLE_SUPER_ADMIN'));
//        dump($this->security);
//        dump($event);
//        exit;
    	//var_dump($event->getRequest());
    	$agent = null;
    	$user = $this->security->getToken() ?$this->security->getToken()->getUser(): '';
    	if(!is_object($user)){
    		return ;
    	}
        
        $request = $event->getRequest();
        $path = $request->getPathInfo();

        if ( $path == 'admin_mfarm_finance_loanapplication_create'){
            $bankId = $event->getRequest()->get('bankProductId');

            return $this->redirect($this->generateUrl('loanapplication_create'));
        }
        
        if($path == '/admin/login' && $this->auth->isGranted('ROLE_SUPER_ADMIN')){
             $event->setResponse(new RedirectResponse(
                                    $this->container->get('router')
                                            ->generate(
                                                    'sonata_admin_dashboard')));
             return;
            //return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }elseif($path == '/login'){
             $event->setResponse(new RedirectResponse(
                                    $this->container->get('router')
                                            ->generate(
                                                    'sonata_admin_dashboard')));
            
            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }
//    	if($this->context->isGranted('ROLE_AGENT')){
//    		$agent = $user;
//    		$user = $user->getParent();
//    	}
//   		throw new AccessDeniedException('Access denied');
//    	$parentId = $request->get('parentId');
//    	$id = $request->get('id');
//    	$path = $request->getPathInfo();

//    	if(($parentId && $id) || $parentId ) {
//    		$parentName = stristr( str_replace('/api/', '', $path) , '/', true );
//    		if($parentName == 'farmers'){
//    			$farmer = $this->om->getRepository('FarmerBundle:Farmer')->findBy(array('user'=>$user, 'id'=>$parentId));
//    			if(!$farmer){
//    				throw new AccessDeniedException('Access denied');
//    			}
//    		}
//
//    		if($parentName == 'users'){
//    			$curentUser = $this->om->getRepository('UserBundle:User')->find($parentId);
//    			if($user->getId() != $curentUser->getId()){
//    				throw new AccessDeniedException('Access denied');
//    			}
//    		}
//    	}elseif($id){
//    		$name = stristr( str_replace('/api/', '', $path) , '/', true );
//    		if($name == 'farmers'){
//    			$farmer = $this->om->getRepository('FarmerBundle:Farmer')->findBy(array('user'=>$user, 'id'=>$id));
//    			if(!$farmer){
//    				throw new AccessDeniedException('Access denied');
//    			}
//    		}
//    		if($name == 'users'){
//    			$curentUser = $this->om->getRepository('UserBundle:User')->find($id);
//
//    			if(!is_object($curentUser)){
//    				throw new AccessDeniedException('Access denied');
//    			}
//    			if($agent){
//    				if($curentUser->getId() != $agent->getId()){
//	    				throw new AccessDeniedException('Access denied');
//	    			}
//    			}elseif($curentUser->getId() != $user->getId()){
//    				throw new AccessDeniedException('Access denied');
//    			}
//    		}
//    	}else{
//
//            $name = str_replace('/api/', '', $path);
//            if($name == 'extensionparameters'){ 
//                if($this->context->isGranted('ROLE_AGENT')){
//                    throw new AccessDeniedException('Access denied');
//                }
//            }
//        }
    }
}