<?php

namespace Mfarm\UserBundle\Services;
 
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
 
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    
    protected $router;
    
    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    
    public function onLogoutSuccess(Request $request)
    {
        // redirect the user to where they were before the login process begun.
//        $referer_url = $this->router->generate('frontend_homepage');
        $response = new RedirectResponse();        
        return $response;
    }
    
}