<?php

namespace Mfarm\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="Mfarm\UserBundle\Repository\UserCreditDetailRepository")
 * @ORM\Table(name="user_credit_detail")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UserCreditDetail
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\UserBundle\Entity\User", inversedBy="userCreditDetail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     *
     * @ORM\Column(name="credit", type="string", length=255, nullable=false)
     * @Expose
     */
    private $credit;

    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Expose
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Expose
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString(){
        return ''.$this->credit;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set credit
     *
     * @param string $credit
     * @return UserCreditDetail
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return string 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return UserCreditDetail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return UserCreditDetail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return UserCreditDetail
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set user
     *
     * @param \Mfarm\UserBundle\Entity\User $user
     * @return UserCreditDetail
     */
    public function setUser(\Mfarm\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Mfarm\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCreditDetail = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userCreditDetail
     *
     * @param \Mfarm\UserBundle\Entity\UserCreditDetail $userCreditDetail
     * @return UserCreditDetail
     */
    public function addUserCreditDetail(\Mfarm\UserBundle\Entity\UserCreditDetail $userCreditDetail)
    {
        $this->userCreditDetail[] = $userCreditDetail;

        return $this;
    }

    /**
     * Remove userCreditDetail
     *
     * @param \Mfarm\UserBundle\Entity\UserCreditDetail $userCreditDetail
     */
    public function removeUserCreditDetail(\Mfarm\UserBundle\Entity\UserCreditDetail $userCreditDetail)
    {
        $this->userCreditDetail->removeElement($userCreditDetail);
    }

    /**
     * Get userCreditDetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserCreditDetail()
    {
        return $this->userCreditDetail;
    }
}
