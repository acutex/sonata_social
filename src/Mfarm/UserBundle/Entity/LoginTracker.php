<?php

namespace Mfarm\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Mfarm\ReceiptBundle\Model\AgentInterface;
//use Ma27\ApiKeyAuthenticationBundle\Annotation as Auth;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sonata\ReceiptBundle\Entity\BaseReceipt as BaseReceipt;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
//use Cunningsoft\ChatBundle\Entity\AuthorInterface;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\SerializedName;



/**
 *
 * @ORM\Entity
 * @ORM\Table(name="login_tracker")

 */
class LoginTracker  {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\UserBundle\Entity\User",inversedBy="loginTracker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return LoginTracker
     */
    public function setCreatedAt(\DateTime $createdAt = NULL) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Mfarm\UserBundle\Entity\User $user
     * @return LoginTracker
     */
    public function setUser(\Mfarm\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Mfarm\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

}
