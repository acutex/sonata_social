<?php

namespace Mfarm\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Mfarm\UserBundle\Model\AgentInterface;
//use Ma27\ApiKeyAuthenticationBundle\Annotation as Auth;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
//use Cunningsoft\ChatBundle\Entity\AuthorInterface;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\SerializedName;



/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Mfarm\UserBundle\Repository\AgentRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 * @UniqueEntity("mobileNo")
 * @UniqueEntity("url")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;



    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;

    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;



    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Serializer\Exclude()
     */
    protected $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Serializer\Exclude()
     */
    protected $groups;

//    /**
//     *
//     * @ORM\ManyToOne(targetEntity="\Mfarm\ActorBundle\Entity\Shop")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
//     * })
//     */
//    private $shop;

//    /**
//     * @Expose
//     * @Accessor(getter="getShopId")
//     * @SerializedName("shop")
//     */
//    private $shopId;
//
//    /**
//     * Get getShopId
//     *
//     */
//    public function getShopId() {
//        return $this->shop ? $this->shop->getId() : null;
//    }
//
//    /**
//     * Set shop.
//     *
//     * @param \Mfarm\ActorBundle\Entity\Shop|null $shop
//     *
//     * @return Sale
//     */
//    public function setShop(\Mfarm\ActorBundle\Entity\Shop $shop = null)
//    {
//        $this->shop = $shop;
//
//        return $this;
//    }
//
//    /**
//     * Get shop.
//     *
//     * @return Shop|null
//     */
//    public function getShop()
//    {
//        return $this->shop;
//    }
//
//    /**
//     * Get shop.
//     *
//     * @return Shop|null
//     */
//    public function getShopName()
//    {
//        return $this->shop?$this->shop->getBusinessName():'No Biz';
//    }

    /**
     * 
     * @Assert\NotBlank(message = "Password can not be blank!!", groups="required")
     * @ORM\Column(name="plain_password", type="string", nullable=true)
     *  Add a comment to this line
     * @Serializer\Exclude()
     */
    private $plainpassword;

    

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;



    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\UserBundle\Entity\DeviceIdentity", mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $deviceIdentity;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude()
     */
    private $country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id",nullable=true)
     * })
     * @Serializer\Exclude()
     */
    private $region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\LocationBundle\Entity\District")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id",nullable=true)
     * })
     * @Serializer\Exclude()
     */
    private $district;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Mfarm\LocationBundle\Entity\Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",nullable=true)
     * })
     * @Serializer\Exclude()
     */
    private $town;
    
        /**
     *
     * @ORM\ManyToOne(targetEntity="\Mfarm\LocationBundle\Entity\Village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="village_id", referencedColumnName="id",nullable=true)
     * })
     * @Serializer\Exclude()
     */
    private $village;

    /**
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @Serializer\Exclude()
     */
    private $name;

    /**
     * @Assert\Type(type="numeric", message="actor.error.mobile_no_onlynumbers", groups={"default_actor"})
     * @Assert\Length(min= 8, max= 15, maxMessage="actor.error.mobile_no_maxlength", minMessage="actor.error.mobile_no_minlength", groups={"default_actor"})
     * @ORM\Column(name="mobile_no", type="string", length=20 , nullable=true)
     * @Serializer\Exclude()
     */
    private $mobileNo;

    /**
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     * @Serializer\Exclude()
     */
    private $address;

    /**
     * @ORM\Column(name="pix", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $pix;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="actor_image", fileNameProperty="pix")
     * 
     * @var File
     * @Serializer\Exclude()
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, name="logo_name",nullable=true)
     *
     * @var string $imageName
     * @Serializer\Exclude()
     */
    protected $logoName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.

     * @Vich\UploadableField(mapping="logo", fileNameProperty="logoName")
     * 
     * @var File $imageFile
     * @Serializer\Exclude()
     */
    protected $logoFile;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Commodity
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setLogoFile(File $logo = null) {
        $this->logoFile = $logo;

        if ($logo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getLogoFile() {
        return $this->logoFile;
    }

    /**
     * @param string $logoName
     */
    public function setLogoName($logoName) {
        $this->logoName = $logoName;
    }

    /**
     * @return string
     */
    public function getLogoName() {
        return $this->logoName ? $this->logoName : 'null';
    }

    /**
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $url;

    /**
     * @ORM\Column(name="public_name", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $publicName;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=255,nullable=true)
     * @Serializer\Exclude()
     */
    private $userType;

    /**
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $latitude;

    /**
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $longitude;




    /**
     * @var decimal
     *
     * @ORM\Column(name="account_balance", type="float", length=255, precision=7, scale=2, options={"default" : 0})
     */
    private $accountBalance = 0;




    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }

    public function setPlainpassword($password) {
        $this->plainpassword = $password;

        return $this;
    }

    public function getPlainpassword() {
        return $this->plainpassword;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt = NULL) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt(\DateTime $updatedAt=null) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set parent
     *
     * @param \Mfarm\UserBundle\Entity\User $parent
     * @return User
     */
    public function setParent(\Mfarm\UserBundle\Entity\User $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Mfarm\UserBundle\Entity\User 
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set mobileNo
     *
     * @param string $mobileNo
     * @return User
     */
    public function setMobileNo($mobileNo) {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo
     *
     * @return string 
     */
    public function getMobileNo() {
        return $this->mobileNo;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set pix
     *
     * @param string $pix
     * @return User
     */
    public function setPix($pix) {
        $this->pix = $pix;

        return $this;
    }

    /**
     * Get pix
     *
     * @return string 
     */
    public function getPix() {
        return $this->pix;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return User
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return User
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Set country
     *
     * @param \Mfarm\LocationBundle\Entity\Country $country
     * @return User
     */
    public function setCountry(\Mfarm\LocationBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Mfarm\LocationBundle\Entity\Country 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param \Mfarm\LocationBundle\Entity\Region $region
     * @return User
     */
    public function setRegion(\Mfarm\LocationBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Mfarm\LocationBundle\Entity\Region 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param \Mfarm\LocationBundle\Entity\District $district
     * @return User
     */
    public function setDistrict(\Mfarm\LocationBundle\Entity\District $district = null) {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \Mfarm\LocationBundle\Entity\District 
     */
    public function getDistrict() {
        return $this->district;
    }

    /**
     * Set town
     *
     * @param \Mfarm\LocationBundle\Entity\Town $town
     * @return User
     */
    public function setTown(\Mfarm\LocationBundle\Entity\Town $town = null) {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \Mfarm\LocationBundle\Entity\Town 
     */
    public function getTown() {
        return $this->town;
    }



    /**
     *
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     *
     * @Accessor(getter="getupdated")
     * @Expose
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     *
     */
    private $deletedat;

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->createdAt);
    }

    /**
     * Get getCountryIds
     *
     */
    public function getupdated() {
        return $timestamp = \Mfarm\ActorBundle\Handler\ApiConfiguration::DatetimeToTimestamp($this->updatedAt);
    }

    /**
     */
    protected $username;

    /**
     */
    protected $password;

    public function setLatLng($latlng) {
        $this->setLatitude($latlng['lat']);
        $this->setLongitude($latlng['lng']);
        return $this;
    }

    public function getLatLng() {
        return array('lat' => $this->getLatitude(), 'lng' => $this
                    ->getLongitude());
    }

    /**
     * @Expose
     * @Accessor(getter="getPixUrl",setter="setPixUrl")
     */
    private $pixUrl;

    /**
     * Get userId
     *
     */
    public function getPixUrl() {
//        return $this->getUploadDir();
        return $this->pix ? $this->getUploadDir() . 'actor/' . $this->pix : $this->getUploadDir() . "default/default_avatar.png";
    }

    protected function getUploadDir() {

        return 'https://' . $_SERVER['SERVER_NAME'] . '/uploads/';
    }

    /**
     * @Expose
     * @Accessor(getter="getParentIds")
     * @SerializedName("user")
     */
    private $userId;

    /**
     * Get actorGroup
     *
     */
    public function getParentIds() {
        return $this->parent ? $this->parent->getId() : null;
    }

    /**
     * Add deviceIdentity
     *
     * @param \Mfarm\UserBundle\Entity\DeviceIdentity $deviceIdentity
     * @return User
     */
    public function addDeviceIdentity(\Mfarm\UserBundle\Entity\DeviceIdentity $deviceIdentity) {
        $this->deviceIdentity[] = $deviceIdentity;
    }

    /**
     * Set userType
     *
     * @param string $userType
     * @return User
     */
    public function setUserType($userType) {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Remove deviceIdentity
     *
     * @param \Mfarm\UserBundle\Entity\DeviceIdentity $deviceIdentity
     */
    public function removeDeviceIdentity(\Mfarm\UserBundle\Entity\DeviceIdentity $deviceIdentity) {
        $this->deviceIdentity->removeElement($deviceIdentity);
    }

    /**
     * Get deviceIdentity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeviceIdentity() {
        return $this->deviceIdentity;
    }

    /** Get userType
     *
     * @return string
     */
    public function getUserType() {
        return $this->userType;
    }


    public function getWarehouse() {
        return $this->warehouse;
    }

//    /**
//     * Add warehouse
//     *
//     * @param \Mfarm\WarehouseBundle\Entity\Warehouses $warehouse
//     * @return User
//     */
//    public function addLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit[] = $loanCredit;
//
//        return $this;
//    }
//
//    /**
//     * Remove warehouse
//     *
//     * @param \Mfarm\WarehouseBundle\Entity\Warehouses $warehouse
//     */
//    public function removeLoanCredit(\Mfarm\LoanBundle\Entity\LoanCredit $loanCredit) {
//        $this->loanCredit->removeElement($loanCredit);
//    }
//
//    /**
//     * Get warehouse
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getLoanCredit() {
//        return $this->loanCredit;
//    }


    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl() {
        return $this->url;
    }


    /**
     * Set Url
     *
     * @param string $Url
     * @return User
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Set accountBalance
     *
     * @param string $accountBalance
     * @return User
     */
    public function setAccountBalance($accountBalance) {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return string 
     */
    public function getAccountBalance() {
        return $this->accountBalance;
    }



    /**
     * Set gender
     * @param string $gender
     * @return User
     */
    public function setGender($gender) {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender() {
        return $this->gender;
    }


    /**
     * Set village
     *
     * @param \Mfarm\LocationBundle\Entity\Village $village
     * @return User
     */
    public function setVillage(\Mfarm\LocationBundle\Entity\Village $village = null)
    {
        $this->village = $village;

        return $this;
    }

    /**
     * Get village
     *
     * @return \Mfarm\LocationBundle\Entity\Village 
     */
    public function getVillage()
    {
        return $this->village;
    }

    /**
     * Set publicName
     *
     * @param string $publicName
     * @return User
     */
    public function setPublicName($publicName)
    {
        $this->publicName = $publicName;

        return $this;
    }

    /**
     * Get publicName
     *
     * @return string 
     */
    public function getPublicName()
    {
        return $this->publicName;
    }



    /**
     *
     * @ORM\OneToMany(targetEntity="\Mfarm\UserBundle\Entity\LoginTracker", mappedBy="user")
     */
    private $loginTracker;

    /**
     * Add loginTracker
     *
     * @param LoginTracker $loginTracker
     * @return User
     */
    public function addLoginTracker(LoginTracker $loginTracker) {
        $this->loginTracker[] = $loginTracker;

        return $this;
    }

    /**
     * Remove loginTracker
     *
     * @param LoginTracker $loginTracker
     */
    public function removeLoginTracker(LoginTracker $loginTracker) {
        $this->loginTracker->removeElement($loginTracker);
    }

    /**
     * Get actor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLoginTracker() {
        return $this->loginTracker;
    }

    /**
     * @ORM\Column(name="demo", type="boolean", nullable=true)
     * @Expose
     */
    private $demo = false;

    /**
     * Set demo
     *
     * @param string $demo
     * @return User
     */
    public function setDemo($demo) {
        $this->demo = $demo;

        return $this;
    }

    /**
     * Get demo
     *
     * @return boolean
     */
    public function getDemo() {
        return $this->demo;
    }
    /**
     * @ORM\Column(name="supported_by",type="string", length=255, nullable=true)
     * @Expose
     */
    private $support;

    /**
     * Set demo
     *
     * @param string $demo
     * @return User
     */
    public function setSupport($support) {
        $this->support = $support;

        return $this;
    }

    /**
     * Get demo
     *
     * @return boolean
     */
    public function getSupport() {
        return $this->support;
    }

    /**
     * @ORM\Column(name="mVersion", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $mVersion;

    /**
     * Set mVersion
     *
     * @param string $mVersion
     * @return User
     */
    public function setMVersion($mVersion) {
        $this->mVersion = $mVersion;

        return $this;
    }

    /**
     * Get mVersion
     *
     * @return string
     */
    public function getMVersion() {
        return $this->mVersion;
    }

    /**
     * Set facebookId.
     *
     * @param string|null $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId = null)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebookId.
     *
     * @return string|null
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebookAccessToken.
     *
     * @param string|null $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken = null)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken.
     *
     * @return string|null
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }
}
