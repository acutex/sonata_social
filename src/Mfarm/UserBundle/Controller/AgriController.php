<?php
namespace Mfarm\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AgriController extends Controller
{
    public function listAction()
    {
        return $this->render('ReportBundle:Dashboard:feaReport.html.twig', [


        ]);
    }

    private function getOptions()
    {
        return $this->container->get('dashboard.Service.handler')->getOptions();
    }

    public function getFarmerRegAction()
    {


        $options = $this->getOptions();

        $parents = [];

        $farmerRegisteredData = $this->getDoctrine()->getRepository('UserBundle:User')->countFarmerRegistered($options);

        foreach ($farmerRegisteredData as $farmerRegisteredChild) {

            $add = null;

            if (!$farmerRegisteredChild['parent_id']){
                $add = $farmerRegisteredChild['id'];
            }else{
                $add = $farmerRegisteredChild['parent_id'];
            }

            if (!array_key_exists($add, $parents)){
                $parents[$add]['farmerRegistered'] = $farmerRegisteredChild['data'];
                $parents[$add]['name'] = $farmerRegisteredChild['name'];
            }else{
                if (isset($parents[$add]['farmerRegistered'])){

                    $parents[$add]['farmerRegistered'] += $farmerRegisteredChild['data'];
                }else{

                    $parents[$add]['farmerRegistered'] = $farmerRegisteredChild['data'];
                }
            }
        }


        $forecastData = $this->getDoctrine()->getRepository('UserBundle:User')->countActorForecast($options);

        foreach ($forecastData as $forecastChild) {

            $add = null;

            if (!$forecastChild['parent_id']){
                $add = $forecastChild['id'];
            }else{
                $add = $forecastChild['parent_id'];
            }

            if (!array_key_exists($add, $parents)){
                $parents[$add]['forecast'] = $forecastChild['data'];
                $parents[$add]['name'] = $forecastChild['name'];
            }else{
                if (isset($parents[$add]['forecast'])){

                    $parents[$add]['forecast'] += $forecastChild['data'];
                }else{

                    $parents[$add]['forecast'] = $forecastChild['data'];
                }
            }
        }


        $transactData = $this->getDoctrine()->getRepository('UserBundle:User')->countActorTransaction($options);

        foreach ($transactData as $transactChild) {

            $add = null;

            if (!$transactChild['parent_id']){
                $add = $transactChild['id'];
            }else{
                $add = $transactChild['parent_id'];
            }

            if (!array_key_exists($add, $parents)){
                $parents[$add]['transact'] = $transactChild['data'];
                $parents[$add]['name'] = $transactChild['name'];
            }else{
                if (isset($parents[$add]['transact'])){

                    $parents[$add]['transact'] += $transactChild['data'];
                }else{

                    $parents[$add]['transact'] = $transactChild['data'];
                }
            }
        }

        $aggregateData = $this->getDoctrine()->getRepository('UserBundle:User')->countActorAggregate($options);

        foreach ($aggregateData as $aggregateChild) {

            $add = null;

            if (!$aggregateChild['parent_id']){
                $add = $aggregateChild['id'];
            }else{
                $add = $aggregateChild['parent_id'];
            }

            if (!array_key_exists($add, $parents)){
                $parents[$add]['aggregate'] = $aggregateChild['data'];
                $parents[$add]['name'] = $aggregateChild['name'];
            }else{
                if (isset($parents[$add]['aggregate'])){

                    $parents[$add]['aggregate'] += $aggregateChild['data'];
                }else{

                    $parents[$add]['aggregate'] = $aggregateChild['data'];
                }
            }
        }

        $loginCountData = $this->getDoctrine()->getRepository('UserBundle:User')->countUserLogin($options);

        foreach ($loginCountData as $loginCountChild) {

            $add = null;

            if (!$loginCountChild['parent_id']){
                $add = $loginCountChild['id'];
            }else{
                $add = $loginCountChild['parent_id'];
            }

            if (!array_key_exists($add, $parents)){
                $parents[$add]['loginCount'] = $loginCountChild['data'];
                $parents[$add]['name'] = $loginCountChild['name'];
            }else{
                if (isset($parents[$add]['loginCount'])){

                    $parents[$add]['loginCount'] += $loginCountChild['data'];
                }else{

                    $parents[$add]['loginCount'] = $loginCountChild['data'];
                }
            }
        }



        $loginData = $this->getDoctrine()->getRepository('UserBundle:User')->lastLogin($options);
        foreach ($loginData as $aggregateChild) {
            $add = null;
            $add = $aggregateChild['id'];

            if (!array_key_exists($add, $parents)){
                $parents[$add]['lastLogin'] = $aggregateChild['data'];
                $parents[$add]['name'] = $aggregateChild['name'];
            }else{
                $parents[$add]['lastLogin'] = $aggregateChild['data'];
                $parents[$add]['name'] = $aggregateChild['name'];
            }

        }



        $formattedData = [];

        foreach ($parents as $parentInfo){

            $dump = [
                "Agribusiness" => $parentInfo['name'],
                "Last Login Date" => isset($parentInfo['lastLogin']) ? $parentInfo['lastLogin'] : null,
                "No Times Logged In" => isset($parentInfo['loginCount']) ? $parentInfo['loginCount'] : null,
                "Farmers Registered" => isset($parentInfo['farmerRegistered']) ? $parentInfo['farmerRegistered'] : null,
                "Forecast Made" => isset($parentInfo['forecast']) ? $parentInfo['forecast'] : null,
                "Services & Inputs Made" => isset($parentInfo['transact']) ? $parentInfo['transact'] : null,
                "Aggregation" => isset($parentInfo['aggregate']) ? $parentInfo['aggregate'] : null
            ];

            $formattedData[] = $dump;
        }

        $formattedData = json_encode($formattedData);

        return $this->render('ReportBundle:Report:reg-table.html.twig', array(
            'id' => 30,
            'objects' => $formattedData,
            'title' => 'FEA Report',
            'headers' => [
                'Agribusiness', 'Last Login Date','No Times Logged In','Farmers Registered',
               'Forecast Made', 'Services & Inputs Made','Aggregation'
            ],
            'credits' => 'mAccess',
        ));

    }

	
}