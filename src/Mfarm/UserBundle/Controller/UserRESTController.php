<?php

namespace Mfarm\UserBundle\Controller;

use Mfarm\UserBundle\Entity\User;
use Mfarm\UserBundle\Form\UserType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * User controller.
 * @RouteResource("User")
 */
class UserRESTController extends VoryxController {

    /**
     * Get a User entity
     * @ApiDoc(
     *  resource=true,
     *  description="get an Period entity"
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(User $entity) {
        return $entity;
    }

    /**
     * Get all User entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, map=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, map=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
//        dump($this->get('request_stack')->getCurrentRequest('headers'));
//        exit;
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
//            $entities = $em->getRepository('LocationBundle:Country')->findBy($filters, $order_by, $limit, $offset);
            $entities = $em->getRepository('UserBundle:User')->findBy($filters, $order_by, null, $offset);
            if ($entities) {
                return $entities;
            }

            return FOSView::create('Not Found', Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a User entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Create a User entity",
     *  parameters={
     * {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     * {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     * {"name"="address", "dataType"="string", "required"=false, "description"="address"},
     * {"name"="mobileNo", "dataType"="string", "required"=false, "description"="Mobile Number"},
     * {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     * {"name"="plainpassword", "dataType"="integer", "required"=false, "description"="plain password"},
     * {"name"="pix", "dataType"="file", "required"=false, "description"="image upload of actor"},
     * {"name"="latitude", "dataType"="decimal", "required"=false, "description"="gps latitude"},
     * {"name"="longitude", "dataType"="decimal", "required"=false, "description"=" gps longitude"},
     * {"name"="country", "dataType"="integer", "required"=false, "description"="country id "},
     * {"name"="region", "dataType"="integer", "required"=true, "description"="region id "},
     * {"name"="district", "dataType"="integer", "required"=true, "description"="district id "},
     * {"name"="town", "dataType"="integer", "required"=true, "description"="town id "},
     * {"name"="commodityIds", "dataType"="integer", "required"=false, "description"="array option(many to many) "},
     * {"name"="parent", "dataType"="integer", "required"=true, "description"="logged in user id"},
     * {"name"="gender", "dataType"="string", "required"=true, "description"="Gender Male/Female"}
     *  }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request) {
        $content = $this->getRequest()->request->all();
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            if (isset($content['pix'])) {
                $photo = $this->setImage($content);
                $entity->setPix($photo);
            }
//            $photo = $this->setImage($content);
//            $entity->setPix($photo);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return array('data' => $entity, 'status' => true);
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update a User entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Update a User entity",
     *  parameters={
     * {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     * {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     * {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     * {"name"="address", "dataType"="string", "required"=false, "description"="address"},
     * {"name"="mobileNo", "dataType"="string", "required"=false, "description"="Mobile Number"},
     * {"name"="plainpassword", "dataType"="string", "required"=false, "description"="plain password"},
     * {"name"="pix", "dataType"="file", "required"=false, "description"="image upload of actor"},
     * {"name"="latitude", "dataType"="decimal", "required"=false, "description"="gps latitude"},
     * {"name"="longitude", "dataType"="decimal", "required"=false, "description"=" gps longitude"},
     * {"name"="country", "dataType"="integer", "required"=false, "description"="country id "},
     * {"name"="region", "dataType"="integer", "required"=true, "description"="region id "},
     * {"name"="district", "dataType"="integer", "required"=true, "description"="district id "},
     * {"name"="town", "dataType"="integer", "required"=true, "description"="town id "},
     * {"name"="parent", "dataType"="integer", "required"=true, "description"="logged in user id"},
     * {"name"="gender", "dataType"="string", "required"=true, "description"="Gender Male/Female"}
     *  }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function putAction(Request $request, User $entity) {
        try {
            $content = $this->getRequest()->request->all();
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new UserType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {

                if (isset($content['pix'])) {
                    $photo = $this->setImage($content);
                    $entity->setPix($photo);
                }
                $em->flush();

                return array('data' => $entity, 'status' => true);
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Partial Update to a User entity.
     * @ApiDoc(
     *  resource=true,
     *  description="Update a User entity",
     *  parameters={
     * {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     * {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     * {"name"="address", "dataType"="string", "required"=false, "description"="address"},
     * {"name"="mobileNo", "dataType"="string", "required"=false, "description"="Mobile Number"},
     * {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     * {"name"="plainpassword", "dataType"="string", "required"=false, "description"="plain password"},
     * {"name"="pix", "dataType"="file", "required"=false, "description"="image upload of actor"},
     * {"name"="latitude", "dataType"="decimal", "required"=false, "description"="gps latitude"},
     * {"name"="longitude", "dataType"="decimal", "required"=false, "description"=" gps longitude"},
     * {"name"="country", "dataType"="integer", "required"=false, "description"="country id "},
     * {"name"="region", "dataType"="integer", "required"=true, "description"="region id "},
     * {"name"="district", "dataType"="integer", "required"=true, "description"="district id "},
     * {"name"="town", "dataType"="integer", "required"=true, "description"="town id "},
     * {"name"="parent", "dataType"="integer", "required"=true, "description"="logged in user id"},
     * {"name"="gender", "dataType"="string", "required"=true, "description"="Gender Male/Female"}
     *  }
     * )
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function patchAction(Request $request, User $entity) {
        return $this->putAction($request, $entity);
    }

    /**
     * Delete a User entity.
     * @ApiDoc(
     *  resource=true,
     * )
     * @View(statusCode=204)

     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function deleteAction(Request $request, User $entity) {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return array('status' => true, 'data' => sprintf('entity with id %d removed', $entity->getId()));
            ;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function setImage($imgJson = null, $tableName = null) {

        $bytes = $imgJson['pix'];
        $fileName = \date('Y-m-d') . '_' . time();

        $filename = $this->createBytesArrayToImage($bytes, "uploads/actor/", str_replace(' ', '_', $fileName));
        return $filename;
    }

    private function createBytesArrayToImage($byteArray, $path, $fileName = null, $fileType = null) {
        $data = base64_decode($byteArray);
        if ($data != "") {
            $im = imagecreatefromstring($data);
            $filename = \date('Y-m-d') . '_' . time() . '_' . $fileName . ".jpeg";

            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }

            //save the image to the disk
            if (isset($im) && $im != false) {
                $imgFile = $path . $filename;

                //delete the file if it already exists
                if (file_exists($imgFile)) {
                    unlink($imgFile);
                }
//                if ($fileType == 'image/jpeg') {
                $result = imagejpeg($im, $imgFile);
//                } else
//                if ($fileType == 'image/png') {
//                    $result = imagepng($im, $imgFile);
//                }
                imagedestroy($im);
                return $filename;
            } else {
                return false;
            }
        }
    }

}
