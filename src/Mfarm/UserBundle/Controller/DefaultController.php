<?php
namespace Mfarm\UserBundle\Controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Paul Gyamfi Fordjour | Ghana



use Mfarm\UserBundle\Form\UserType;
use Mfarm\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function signUpAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $userForm = $this->createForm(new userType());
        $handler = $this->container->get('wafp.com.commonfunction');


        $fullName = $request->request->get('fullname');
        $email = $request->request->get('email');
        $contact = $request->request->get('contact');
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $userExist = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
        $emailExist = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($email);

        $response = '';
        $error = null;
        $user = new User();
        if ($request->isMethod('POST')) {
            if (($userExist == null) && ($emailExist == null)) {
                $error = 0;
                $user->setName($fullName);
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setPlainpassword($password);
                $user->setPassword($password);
                $user->setMobileNo($contact);
                $user->setEmail($email);
                $user->getUserType('aggregator');
                $user->setEnabled(1);
                
                // set default group for aggregator
                $repository = $em->getRepository('ApplicationSonataUserBundle:Group');
                $group = $repository->findOneByName('Aggregator');
                $group?$user->addGroup($group):null;
                $user->addRole('ROLE_ADMIN');
                $em->persist($user);
                $em->flush();

                $countryCode = substr($contact, 0, 3);
                $em = $this->getDoctrine()->getManager();
//                $results = $em->getRepository("SettingBundle:SignUpTemplate")->getSignUpTemplateByCountry($countryCode);

//                [Name] 
                if (!empty($results)) {
                    $body = str_replace("[Name]", $fullName, $results['content']);
                    $heading = $results['heading'];
                    $subject = $results['subject'];
                    $replyTo = explode(",", $results['email']);
                    $emails = $results['email'];
                    $mobileNo = $results['mobileNo'];
                    $services = $results['service'];
                }
                $replyTo = $replyTo ? (array_combine($replyTo, $replyTo)) : "";
//                
                $send = $this->sendEmail($subject, $body, $email, $replyTo, $mobileNo, $services, $heading, 1);

//                dump($send);
//                exit;
                $response = "Thank You <i>" . $username . "</i> For Signing Up Please Login Now ";
            } elseif ($userExist != null) {
                $error = 1;
                $response = 'Sorry username <i>' . $username . '</i> already Taken';
            } else {
                $error = 1;
                $response = 'Sorry email <i>' . $email . '</i> Already Exist';
            }
        }

        return $this->render('UserBundle:Default:signUp.html.twig', array(
                    'userForm' => $userForm->createView(),
                    'response' => $response,
                    'error' => $error,
                    'fullname' => $fullName,
                    'username' => $username,
                    'contact' => $contact,
                    'email' => $email,
        ));
    }

    public function checkTemplateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
//        $results = $em->getRepository("SettingBundle:SignUpTemplate")->getSignUpTemplateByCountry('233');
//

        if (!empty($results)) {
            $content = $results['content'];
            $heading = $results['heading'];
            $subject = $results['subject'];
            $email = $results['email'];
            $mobileNo = $results['mobileNo'];
            $services = $results['service'];
        }


        return $this->render('Email/signupTemplate.html.twig', array(
                    'content' => $content,
                    'heading' => $heading,
                    'subject' => $subject,
                    'email' => $email,
                    'mobileNo' => $mobileNo,
                    'services' => $results['service'],
        ));
    }

    // function to send email
    public function sendEmail($subject, $body, $to, $replyTo = null, $mobileNo = null, $services = null, $heading = null, $template = null) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($replyTo)
                ->setReplyTo($replyTo)
                ->setTo($to);

        if ($template != null) {
            $message->setBody($this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                            'Email/signupTemplate.html.twig', array(
                        'content' => $body,
                        'sender' => $replyTo,
                        'email' => $replyTo ? implode(', ', $replyTo) : "",
                        'mobileNo' => $mobileNo,
                        'heading' => $heading,
                        'services' => $services,
                            )
                    ), 'text/html');
        } else {
            $message->setBody($body);
        }

        $sent = $this->get('mailer')->send($message);
        if ($sent) {
            return true;
        } else {
            return false;
        }
//        $headers = 'From: <slamadeku@yahoo.com>' . "\r\n";
//        $headers .="Reply-To: Dela <slamadeku@yahoo.com>";
//        $mailer = $this->container->get('mailer');
//
//        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
//                ->setUsername('ggcwarehousemanagement@gmail.com')
//                ->setPassword('dela@imagead.net');
//        $mailer=\Swift_Mailer::newInstance($transport);
//        
//        $message = \Swift_Message::newInstance()
//                ->setSubject($subject)
//                ->setFrom("slamadeku@yahoo.com")
//                ->setReplyTo("slamadeku@yahoo.com")
//                ->setTo($to)
//                ->setBody("sadfasfdsadf");
//        $this->get('mailer')->send($message);
    }

    public function loginAction(Request $request) {

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('UserBundle:Default:login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error' => $error,
        ));
    }

    public function forgetAction(Request $request) {
        $userForm = $this->createForm(new userType());
        $em = $this->getDoctrine()->getManager();
        $nameOrEmail = $request->request->get('username');
        $error = 1;
        $user = null;
        $pass = $this->generateRandomString();

         
            if ($request->isMethod('POST')) {
            $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($nameOrEmail);
            if ($user != null) {
                $data = $em->getRepository('UserBundle:User')->find($user);
                $email = $user->getEmail();
                $data->setPlainPassword($pass);
                $body = 'Your New Password is ' . $pass . ' login and change (Optional)';
                if ($email != null) {
                    $send = $this->sendEmail('Mfarms Password Reset', $body, $email, array('info@mfarms.org'), null, null, $heading = null, null);
                    $em->flush();
                    $error = null;
                } else {
                    $error = 3;
                }
            } else {
                $error = 2;
            }
        }
        return $this->render('UserBundle:Default:forget.html.twig', array(
                    'username' => $nameOrEmail,
                    'error' => $error,
                    'user' => $user,
                    'userForm' => $userForm->createView(),
                        )
        );
    }

    public function generateRandomString($length = 10) {

        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public function getMailSent($send) {
        $error = 3;
        if ($send) {
            $error = null;
        }
    }

}
