<?php

namespace Mfarm\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Mfarm\UserBundle\Exception\InvalidFormException;
use Mfarm\UserBundle\Form\UserType;
use Mfarm\UserBundle\Model\UserInterface;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends FOSRestController {

  /**
   * List all users.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\QueryParam(name="offset", requirements="datetime", nullable=true, description="Offset from which to start listing users.")
   * @Annotations\QueryParam(name="limit", requirements="datetime", default="5", description="How many users to return.")
   *
   * @Annotations\View(serializerEnableMaxDepthChecks=true
   * )
   *
   * @param Request               $request      the request object
   * @param ParamFetcherInterface $paramFetcher param fetcher service
   *
   * @return array
   */
  public function getUsersAction(Request $request, ParamFetcherInterface $paramFetcher)
  {
      return $this->container->get('user.manager')->getCurrentUser();
      //$entity = $this->getOr404($id);
  }

  /**
   * Get single User.
   *
   * @ApiDoc(
   *   resource = true,
   *   description = "Gets a User for a given id",
   *   output = "Mfarm\UserBundle\Entity\User",
   *   statusCodes = {
   *     200 = "Returned when successful",
   *     404 = "Returned when the User is not found"
   *   }
   * )
   *
   * @Annotations\View(templateVar="user",serializerEnableMaxDepthChecks=true)
   *
   * @param int     $id      the user id
   *
   * @return array
   *
   * @throws NotFoundHttpException when user not exist
   */
  public function getUserAction($id)
  {
      exit;
      $entity = $this->getOr404($id);

      return $entity;
  }

  /**
   * Presents the form to use to create a new user.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\View(
   *  templateVar = "form"
   * )
   *
   * @return FormTypeInterface
   */
  public function newUserAction()
  {
      $securityContext = $this->container->get('security.token_storage');
      return $this->createForm(new UserType($securityContext));
  }

  /**
   * Create a User from the submitted data.
   *
   * @ApiDoc(
   *   resource = true,
   *   description = "Creates a new user from the submitted data.",
   *   input = "Mfarm\UserBundle\Form\UserType",
   *   statusCodes = {
   *     200 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:User:newUser.html.twig",
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   *
   * @return FormTypeInterface|View
   */
  public function postUserAction(Request $request)
  {
      try {
      $newEntity = $this->container->get('userbundle.user.handler')->post(
          $request->request->all()
      );

      $routeOptions = array(
          'id' => $newEntity->getId(),
          '_format' => $request->get('_format')
      );
      if($request->get('_format') == 'html' || $request->get('_format') == null)
        return $this->redirect('api_login_form');
      else
        return $this->routeRedirectView('api_login_form', $routeOptions, Codes::HTTP_CREATED);

      } catch (InvalidFormException $exception) {

          return $exception->getForm();
      }
  }

  /**
   * Update existing user from the submitted data or create a new user at a specific location.
   *
   * @ApiDoc(
   *   resource = true,
   *   input = "Mfarm\UserBundle\Form\UserType",
   *   statusCodes = {
   *     201 = "Returned when the User is created",
   *     204 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:User:editUser.html.twig",
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   * @param int     $id      the user id
   *
   * @return FormTypeInterface|View
   *
   * @throws NotFoundHttpException when user not exist
   */
  public function putUserAction(Request $request, $id)
  {
      exit;
      try {
            if (!($entity = $this->container->get('userbundle.user.handler')->get($id))) {
                $statusCode = Codes::HTTP_CREATED;
                $entity = $this->container->get('userbundle.user.handler')->post(
                    $request->request->all()
                );
            } else {
                $statusCode = Codes::HTTP_NO_CONTENT;
                $entity = $this->container->get('userbundle.user.handler')->put(
                    $entity,
                    $request->request->all()
                );
            }

            $routeOptions = array(
                'id' => $entity->getId(),
                '_format' => $request->get('_format')
            );
            if($request->get('_format') == 'html' || $request->get('_format') == null)
              return $this->routeRedirectView('api_userbundle_get_users');
            else
              return $this->routeRedirectView('api_userbundle_get_user', $routeOptions, $statusCode);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
  }

  /**
   * Update existing user from the submitted data or create a new user at a specific location.
   *
   * @ApiDoc(
   *   resource = true,
   *   input = "Mfarm\UserBundle\Form\UserType",
   *   statusCodes = {
   *     204 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:User:editUser.html.twig",
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   * @param int     $id      the user id
   *
   * @return FormTypeInterface|View
   *
   * @throws NotFoundHttpException when user not exist
   */
  public function patchUserAction(Request $request, $id)
  {
      try {
            $securityContext = $this->container->get('security.token_storage');
            $form = $this->createForm(get_class(new UserType($securityContext)));
            $form->bind($request);
            
            $logo =array('logo' => $form['logo']->getData() );
            $allParm = array_merge($request->request->all(),$logo);

            $entity = $this->container->get('userbundle.user.handler')->patch(
                $this->getOr404($id),
                $allParm
            );

            $routeOptions = array(
                'id' => $entity->getId(),
                '_format' => $request->get('_format')
            );
            if($request->get('_format') =='html' || $request->get('_format') == ''){
              return $this->routeRedirectView('api_farmerbundle_get_farmers');
            }
            return $this->routeRedirectView('api_userbundle_get_user', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
  }

  /**
   * Presents the form to use to create a new user.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:User:editUser.html.twig"
   * )
   *
   * @return FormTypeInterface
   */
  public function editUserAction($id)
  {
      if (!($entity = $this->container->get('userbundle.user.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
      }
      $securityContext = $this->container->get('security.token_storage');
      if($securityContext->getToken()->getUser()->getId() != $id){
        throw new AccessDeniedException('Access denied');
      }
      $form = $this->createForm(new UserType($securityContext), $entity);
      return array('form'=>$form, 'entity' => $entity );
  }

  /**
   * Detele user
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   */
  public function deleteUserAction($id)
  {
    exit;
      if (!($entity = $this->container->get('userbundle.user.handler')->delete($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
      }
      return $this->routeRedirectView('api_userbundle_get_users');   
  }

  /**
   * Fetch a <entiry> or throw an 404 Exception.
   *
   * @param mixed $id
   *
   * @return UserInterface
   *
   * @throws NotFoundHttpException
   */
  protected function getOr404($id)
  {
        if (!($entity = $this->container->get('userbundle.user.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $entity;
  }
}
