<?php

namespace Mfarm\UserBundle\Controller;

use Mfarm\UserBundle\Entity\User;
use Mfarm\UserBundle\Form\UserRegistrationForm;
use Mfarm\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserRegistrationController extends Controller
{

    /**
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(UserRegistrationForm::class);

        $form->handleRequest($request);

//        dump($form); exit;

        if ($form->isValid()) {

            /** @var User $user */
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $user->setUserType($this->get('userbundle.user.handler')->getAggregatorType());
            $user->setEnabled(true);
            $roles = array('ROLE_AGGREGATOR');
            $user->setRoles($roles);

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Welcome '.$user->getEmail());

            return $this->redirectToRoute('sonata_admin_dashboard');
        }

        return $this->render('@User/User/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
