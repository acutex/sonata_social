<?php

namespace Mfarm\UserBundle\Listener;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Doctrine\ORM\EntityManager;
use Mfarm\UserBundle\Entity\LoginTracker;
use Doctrine\Common\Persistence\ObjectManager;



class LoginListener {

    protected $userManager;
    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $em = $this->em;

        $event = new LoginTracker();
        $event->setUser($user);

        $em->persist($event);
        $em->flush();

    }
}