<?php

namespace Mfarm\UserBundle\Admin;



use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;



class AgriAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'agri_report';
    protected $baseRouteName = 'agri_report';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}

