<?php

namespace Mfarm\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityRepository;

class UserAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '500M');
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        $selectGroupNames = array(
            'admin',
            'Aggregator',
            'aggregator'
        );

        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getPix();
            $logo = $this->getSubject()->getLogoName();

            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            $logoFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/actor/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = '<img src="' . $fullPath . '" class="admin-preview" />';
            }
            if ($logo) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/logo/' . $logo;

                // add a 'help' option containing the preview's img tag
                $logoFieldOptions['help'] = '<img src="' . $fullPath . '" class="admin-preview" />';
            }
        }

        $formMapper
                ->with('General', array('class' => 'col-md-6'))->end()
                ->with('Profile', array('class' => 'col-md-6'))->end();


        if (($user->getUserName() == 'admin')) {
//            $formMapper->with('Client Page', array('class' => 'col-md-12'))
//                    ->add('url', null, array())
//                    ->add('publicName', null, array("help" => "This is the name that will appear on our website."))
//                    ->add('logoFile', 'file', array(
//                        'label' => 'Logo',
//                        'by_reference' => false,
//                        'data_class' => null,
//                        'required' => false), $logoFieldOptions)
//                    ->end();
        };

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        $countryCode = $user->getCountry() ? $user->getCountry()->getcountryCode() : "";
        $children = $this->getUserChildren();
        $parent = $user->getParent();
        $formMapper
                ->with('General')
                ->add('username')
                ->add('email')
                ->add('mVersion', null, array('label' => 'Mobile App Version'))
                ->add('plainpassword', null, array('label' => 'Plain Password'))
//                ->add('shop', null, array(
//                    'class' => 'MfarmActorBundle:Shop',
//                    'error_bubbling' => true,
//                    'query_builder' => function (EntityRepository $er) use ($children, $parent) {
//
//                        $query = $er->createQueryBuilder('o')
//                                ->leftJoin('o.user', 'u')
//                                ->where('u.id = :user_id')
//                                ->setParameter('user_id', $this->getUser()->getId());
//                        if ($parent != null) {
//                            $query->Orwhere('u.id = :parent_id')
//                            ->setParameter('parent_id', $parent->getId());
//                        }
//                        if (!empty($children)) {
//                            $query->Orwhere($query->expr()->in('o.user', $children));
//                        }
//                        return $query;
//                    }
//                        ), ['admin_code' => 'mfarm_actor.admin.shop']
//                )
                ->add('demo', null, array('label' => 'Demo?'))

        ;

        if (in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            $formMapper
                    ->add('support', 'choice', array('choices' => array(
                            'Made' => 'made',
                            'Other' => 'other',
//                            'district' => 'District Rep',
//                            'regional' => 'Regional Rep',
//                            'country' => 'Country Rep',
                )))
                    ->add('userType', 'choice', array('choices' => array(
                            'Agent' => 'agent',
                            'Aggregator' => 'aggregator',
                            'District Rep' => 'district',
                            'Regional Rep' => 'regional',
                            'Country Rep' => 'country',
                )))
                    ->add('parent', null, array(
                        'placeholder' => 'Select Parent'
                    ))
                    ->add('groups', null, array(
                        'label' => 'User  Abiltity',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => true,
                        'class' => 'ApplicationSonataUserBundle:Group',
                        'error_bubbling' => true,
                        'query_builder' => function(EntityRepository $er) use ($selectGroupNames) {

                            $query = $er->createQueryBuilder('o')
                            ;

                            if (!empty($selectGroupNames)) {
                                $query->Orwhere($query->expr()->notIn('o.name', $selectGroupNames));
                            }
                            return $query;
                        }
                            )
                    )
                    ->add('roles', 'choice', array('choices' => $this->getRoles(), 'multiple' => true))


//                    ->add('parent', null, array())
//                    ->add('roles', 'choice', array('choices' => $this->getRoles(), 'multiple' => true))
//                    ->add('groups', null, array())
//                    ->add('enabled');

            ;
        }

        if ($user->hasRole('ROLE_AGENT')) {
            $formMapper
                    ->add('groups', null, array(
                        'label' => 'User  Abiltity',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => true,
                        'class' => 'ApplicationSonataUserBundle:Group',
                        'error_bubbling' => true,
                        'query_builder' => function (EntityRepository $er) {

                            $query = $er->createQueryBuilder('o')
                                    ->andWhere('o.name = :n')
                                    ->setParameter('n', "FEA");
                            return $query;
                        }
                            )
            );
        }

//
//        if (($user->getUserName() == 'admin')) {
//            $formMapper
////                    ->end()
////                    ->with('Access')
//                ->add('roles', 'choice', array('choices' => $this->getRoles(), 'multiple' => true))
//                ->add('groups', null, array(
//                        'label' => 'User  Abiltity',
//                        'expanded' => true,
//                        'multiple' => true,
//                        'required' => true,
//                        'class' => 'ApplicationSonataUserBundle:Group',
//                        'error_bubbling' => true,
//                        'query_builder' => function(EntityRepository $er) use ($selectGroupNames) {
//
//                            $query = $er->createQueryBuilder('o')
//                            ;
//
//                            if (!empty($selectGroupNames)) {
//                                $query->Orwhere($query->expr()->notIn('o.name', $selectGroupNames));
//                            }
//                            return $query;
//                        }
//                    )
//                )
//                ->add('enabled')
//                ->add('groups', null, array(
//                    'label' => 'User  Abiltity',
//                    'expanded' => true,
//                    'multiple' => true,
//                    'required' => true
//                ));
//        }

        if (($user->getUserType() == 'aggregator')) {
            $formMapper
                    ->add('enabled')
            ;
        }

        $formMapper
                ->add('enabled')
                ->end()
                ->with('Profile')
                ->add('imageFile', 'file', array(
                    'label' => 'Profile Picture',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => false), $fileFieldOptions)
                ->add('name')
                ->add('gender', 'choice', array('choices' => array('Male' => 'M', 'Female' => 'F')))
                ->add('mobileNo', null, array('help' => "Mobile Number should be with country code eg:<b>233xxxxxxxxx </b>"))
                ->add('address')
                ->end()
//
                ->with('Location', array('class' => 'col-md-6'))
//                ->add('country', null, array('required' => true, 'placeholder' => 'Select Country'))
                ->add('region')
                ->add('district', \Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType::class, array('required' => false, 'entity_alias' => 'district_by_region', 'empty_value' => 'Select District'
                    , 'parent_field' => 'region', 'attr' => array('class' => 'select2-offscreen', 'style' => 'width:100%')), array('list' => 'list'))
                ->end()
//                ->add('town', \Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType::class, array('entity_alias' => 'town_by_district', 'empty_value' => 'Select Town'
//                    , 'parent_field' => 'district', 'attr' => array('class' => 'select2-offscreen', 'style' => 'width:100%')), array('list' => 'list'))
//                ->add('village', \Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType::class, array('required' => false, 'entity_alias' => 'village_by_town', 'empty_value' => 'Select Village'
//                    , 'parent_field' => 'town', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
//
                ->with('GPS Location', array('class' => 'col-md-6'))
                ->add('latlng', \Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType::class, array(
                    'required' => false,
                    'label' => 'search here',
                    'lat_options' => array(
                        'label' => 'Latitude',
                        'required' => false,
                        'attr' => array(
                        )), // the options for just the lat field
                    'lng_options' => array('required' => false, 'label' => 'Longitude'), // the options for just the lng field
                    'map_width' => 450,
                    'default_lat' => 5.652623, // the starting position on the map
                    'default_lng' => -0.209072,
                        )
                )
        ;
    }

    public function getNewInstance() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
//        $roles = array('ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_ALLOWED_TO_SWITCH');
        $roles = array('ROLE_ADMIN');
        $instance = parent::getNewInstance();
        $instance->setParent($user);
        $instance->setRoles($roles);


        return $instance;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
//                ->add('actor.pix', null, array(
//                    'template' => 'MfarmActorBundle:Admin:list_pix_preview.html.twig',
//                    'attr' => array('style' => 'width: 44.171%; ')))
                ->add('username')
                ->add('name')
                ->add('mVersion', null, array('label' => 'ver'))
                ->add('demo', null, array('label' => 'Demo'))
//            ->add('parent')
//            ->add('gender', 'choice', array('choices' => array('M' => 'Male', 'F' => 'Female')))
//            ->add('email')
//            ->add('plainpassword')
//            ->add('userType')
//            ->add('mobileNo')
//            ->add('country')
//            ->add('region')
//            ->add('district')
//            ->add('town')
//            ->add('village')
                ->add('groups')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function prePersist($object) {
        //set user type created by aggregator to agent
        if ($this->getUser()->getUsertype() == 'aggregator') {
            $object->setUserType('agent');
            $group = $this->getConfigurationPool()
                    ->getContainer()
                    ->get('Doctrine')
                    ->getRepository("ApplicationSonataUserBundle:Group")
                    ->findBy(array('name' => 'agent'));
            $object->addGroup($group[0]);


            //also set the default group for agent
        }
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('username')
                ->add('parent')
                ->add('name')
                ->add('groups')
                ->add('email')
                ->add('mobileNo')
                ->add('gender')
                ->add('country')
                ->add('region')
                ->add('district')
                ->add('town')
                ->add('village')
                ->add('roles');
        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('actor.pix', null, array(
                    'template' => 'MfarmActorBundle:Admin:list_pix_preview.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')))
                ->add('username')
                ->add('firstName')
                ->add('lastName')
                ->add('email')
                ->add('country')
                ->add('region')
                ->add('district')
                ->add('town')

        ;


        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'UserBundle:Admin:user_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    private function getRoles() {
        $originRoles = self::flattenRoles($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));

        $roles = array();
        $rolesAdded = array();

        // Add herited roles
//        foreach ($originRoles as $roleParent => $rolesHerit) {
//            $tmpRoles = array_values($rolesHerit);
//            $rolesAdded = array_merge($rolesAdded, $tmpRoles);
//            $roles[$roleParent] = array_combine($tmpRoles, $tmpRoles);
//        }
//        $roles=["ROLE_AGENT"=>"ROLE_AGENT",  "ROLE_MADE"=>"ROLE_MADE" ,  "ROLE_USER"=>"ROLE_USER" ,  "ROLE_AGGREGATOR"=>"ROLE_AGGREGATOR", 
//  "ROLE_SUPER_ADMIN"=>"ROLE_SUPER_ADMIN"];
//dump($originRoles);exit;

        return $originRoles;
    }

    /** Turns the role's array keys into string <ROLES_NAME> keys.
     * @todo Move to convenience or make it recursive ? ;-)
     */
    protected static function flattenRoles($rolesHierarchy) {
        $flatRoles = array();
        foreach ($rolesHierarchy as $roles) {

            if (empty($roles)) {
                continue;
            }

            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    $flatRoles[$role] = $role;
                }
            }
        }

        return $flatRoles;
    }

    public function getDefaultOptions() {
        return array(
            'roles' => null
        );
    }

    private function refactorRoles($originRoles) {
        $roles = array();
        $rolesAdded = array();

        // Add herited roles
        foreach ($originRoles as $roleParent => $rolesHerit) {
            $tmpRoles = array_values($rolesHerit);
            $rolesAdded = array_merge($rolesAdded, $tmpRoles);
            $roles[$roleParent] = array_combine($tmpRoles, $tmpRoles);
        }
        // Add missing superparent roles
        $rolesParent = array_keys($originRoles);
        foreach ($rolesParent as $roleParent) {
            if (!in_array($roleParent, $rolesAdded)) {
                $roles['-----'][$roleParent] = $roleParent;
            }
        }

        return $roles;
    }

    public function createQuery($context = 'list') {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        $query = parent::createQuery($context);
//        dump($user->getRoles());
        if (!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->hasRole('ROLE_MADE')) {
            $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
            $query->where('u.parent = :user_id')
                    ->setParameter('user_id', $user->getId());
        }if ($user->hasRole('ROLE_MADE') && !$user->hasRole('ROLE_SUPER_ADMIN')) {
            $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
            $query->where('u.support = :supported_by')
                    ->setParameter('supported_by', 'made');
        }
        
        return $query;
    }

    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        return $user;
    }

    public function preUpdate($user) {
        
    }

    public function getUserChildren() {
        $info = $this->getConfigurationPool()->getContainer()
                ->get('doctrine')->getRepository("UserBundle:User")
                ->getChildren($this->getUser()->getId());
        return $info;
    }

}
