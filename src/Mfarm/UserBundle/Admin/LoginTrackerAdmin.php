<?php

namespace Mfarm\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LoginTrackerAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('user')
//            ->add('createdAt')
        ;
    }


    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->add('id')
            ->add('user')
            ->add('createdAt')
        ;
    }




    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('user')
            ->add('createdAt')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('user')
            ->add('createdAt')
        ;
    }


}
