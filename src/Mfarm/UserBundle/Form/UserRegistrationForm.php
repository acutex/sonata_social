<?php

namespace Mfarm\UserBundle\Form;

use Mfarm\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'attr'=> [
                    'class' => 'form-control',
                    'placeholder' => 'Name'
                ]
            ))
            ->add('username', null, array(
                'attr'=> ['class' => 'form-control',
                    'placeholder' => 'Username']
            ))
            ->add('email', EmailType::class, array(
                'attr'=> ['class' => 'form-control',
                    'placeholder' => 'E-mail']
            ))
            ->add('mobileNo', null, array(
                'label' => 'Mobile Number',
                'attr'=> ['class' => 'form-control',
                    'placeholder' => 'Mobile Number']
            ))
            ->add('plainpassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'attr'=> ['class' => 'form-control']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
