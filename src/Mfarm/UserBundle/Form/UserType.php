<?php

namespace Mfarm\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('createdAt')
                ->add('updatedAt')
                ->add('plainpassword')
                ->add('username')
                ->add('email')
                ->add('name')
                ->add('pix')
                ->add('mobileNo')
                ->add('address')
                ->add('pix')
                ->add('latitude')
                ->add('longitude')
                ->add('parent')
                ->add('country')
                ->add('region')
                ->add('district')
                ->add('town')
                ->add('groups')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mfarm\UserBundle\Entity\User',
            'csrf_protection' => false
            , 'cascade_validation' => true,
                )
        );
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mfarm_userbundle_user';
    }

}
