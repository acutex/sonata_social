$(".adropdown img.flag").addClass("flagvisibility");

$(".adropdown dt a").click(function () {
    $(".adropdown dd ul").toggle();
});

$(".adropdown dd ul li a").click(function () {
    var text = $(this).html();
    $(".adropdown dt a span").html(text);
    $(".adropdown dd ul").hide();
    $("#result").html("Selected value is: " + getSelectedValue("sample"));
});

function getSelectedValue(id) {
    return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("adropdown"))
        $(".adropdown dd ul").hide();
});



$(".adropdown img.flag").toggleClass("flagvisibility");

