$(function () {
    // On document ready, call visualize on the datatable.
    $(document).ready(function () {

        /**
         * Visualize an HTML table using Highcharts. The top (horizontal) header
         * is used for series names, and the left (vertical) header is used
         * for category names. This function is based on jQuery.
         * @param {Object} table The reference to the HTML table to visualize
         * @param {Object} options Highcharts options
         */
        Highcharts.visualize = function (table, options) {
            console.log(table)
            var stack = false;
            // the categories
            options.xAxis.categories = [];
            $('thead th.chartName', table).each(function (i) {
               
                options.xAxis.categories.push(this.innerHTML);
            });

            // the data series
            options.series = [];
            $('thead tr', table).each(function (i) {
                var tr = this;
                
                if (i == 0) {
                    $('th.chartDatas', tr).each(function (j) {
                        
                        options.series[j] = {
                            name: this.innerHTML,
                            data: []
                        }
                        if ($(this).attr('data-stack')) {
                            options.series[j]['stack'] = $(this).attr('data-stack')
                            stack = true;
                        }

                    });
                }
                if (stack) {
                    options.plotOptions = [];
                    options.plotOptions.column = [];
                    options['plotOptions']['column']['stacking'] = 'normal';
                }


            });
            
            $('tbody tr', table).each(function (i) {
                var tr = this;
                console.log('lit'+i)
                if (i == 0) {
                    console.log(  $('td.chartDatas', tr).size())
                    $('td.chartDatas', tr).each(function (j) {
                       console.log(this.innerHTML)
                        options.series[j].data.push(parseFloat(this.innerHTML));
                    });

                }
            });



            if (options.xAxis.categories.length > 0) {
                var chart = new Highcharts.Chart(options);
            }
        }

        var table = document.getElementById('visualizeTable'),
                options = {
                    chart: {
                        renderTo: 'piechart',
                        type: 'column'
                    },
                    title: {
                        text: $('#chartTitle').text()
                    },
                    xAxis: {
                    },
                    yAxis: {
                        title: {
                            text: 'Units'
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return 'In ' + this.x.toLowerCase() + ' ' + this.y + ' ' + this.series.name;
                        }
                    }
                };
                
        Highcharts.visualize(table, options);
    });

});

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
    , format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    }
    return function (table, name, filename) {
        if (!table.nodeType)
            table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

        document.getElementById("dlink").href = uri + base64(format(template, ctx));
        document.getElementById("dlink").download = filename;
        document.getElementById("dlink").click();

    }
})()