/*
 * 	Character Count Plugin - jQuery plugin
 * 	Dynamic character count for text areas and input fields
 *	written by Alen Grakalic	
 *	http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas
 *
 *	Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 */
 
(function($) {

	$.fn.charCount = function(options){
	  
		// default configuration properties
		var defaults = {	
			allowed: 140,		
			warning: 25,
			css: 'counter',
			counterElement: 'span',
			cssWarning: 'warning',
			cssExceeded: 'exceeded',
			counterText: ''
		}; 
			
		var options = $.extend(defaults, options); 
		
		function calculate(obj){
			var count = $(obj).val().length;
			var available = options.allowed - count;
			if(available <= options.warning && available >= 0){
				$(obj).next().addClass(options.cssWarning);
			} else {
				$(obj).next().removeClass(options.cssWarning);
			}
			if(available < 0){
				$(obj).next().addClass(options.cssExceeded);
			} else {
				$(obj).next().removeClass(options.cssExceeded);
			}
			$(obj).next().html(options.counterText + available);
		};
				
		this.each(function() {  			
			$(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
			calculate(this);
			$(this).keyup(function(){calculate(this)});
			$(this).change(function(){calculate(this)});
		});
	  
	};

})(jQuery);


// drag and drop javascript
function insertAtCaret(area, text) {
    var scrollPos = area.scrollTop;
    var strPos = 0;
    var br = ((area.selectionStart || area.selectionStart == '0') ? "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") {
        area.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -(area.value.length));
        strPos = range.text.length;
    } else if (br == "ff")
        strPos = area.selectionStart;
    var front = (area.value).substring(0, strPos);  
    var back = (area.value).substring(strPos, area.value.length); 
    area.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        area.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -(area.value.length));
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    } else if (br == "ff") {
        area.selectionStart = strPos;
        area.selectionEnd = strPos;
        area.focus();
    }
    area.scrollTop = scrollPos;
}

$(document).ready(function() {
    var options = {
        accept: "span.placeholder",       
        drop: function(ev, ui) {
            insertAtCaret($("textarea#short_message_description").get(0), ui.draggable.eq(0).text());
        }
    };

    $("span.placeholder").draggable({
        helper:'clone',
        start: function(event, ui) {
            var txta = $("textarea#short_message_description");
            $("div#pseudodroppable").css({
                position:"absolute",
                top:txta.position().top,
                left:txta.position().left,
                width:txta.width(),
                height:txta.height()
            }).droppable(options).show();
        },
        stop: function(event, ui) {
            $("div#pseudodroppable").droppable('destroy').hide();
        }
    });
});