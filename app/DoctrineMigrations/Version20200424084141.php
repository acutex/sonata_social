<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200424084141 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fos_user_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_583D1F3E5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_C560D76192FC23A8 (username_canonical), UNIQUE INDEX UNIQ_C560D761A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_C560D761C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_credit_detail (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, credit VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_F1BE33A2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_identity (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, device VARCHAR(255) NOT NULL, INDEX IDX_E4A1B0E4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, district_id INT DEFAULT NULL, town_id INT DEFAULT NULL, village_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(1000) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, facebook_id VARCHAR(255) DEFAULT NULL, facebook_access_token VARCHAR(255) DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, plain_password VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, mobile_no VARCHAR(20) DEFAULT NULL, address LONGTEXT DEFAULT NULL, pix VARCHAR(255) DEFAULT NULL, logo_name VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, public_name VARCHAR(255) DEFAULT NULL, user_type VARCHAR(255) DEFAULT NULL, latitude VARCHAR(255) DEFAULT NULL, longitude VARCHAR(255) DEFAULT NULL, account_balance DOUBLE PRECISION DEFAULT \'0\' NOT NULL, demo TINYINT(1) DEFAULT NULL, supported_by VARCHAR(255) DEFAULT NULL, mVersion VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), INDEX IDX_957A6479727ACA70 (parent_id), INDEX IDX_957A6479F92F3E70 (country_id), INDEX IDX_957A647998260155 (region_id), INDEX IDX_957A6479B08FA272 (district_id), INDEX IDX_957A647975E23604 (town_id), INDEX IDX_957A64795E0D5582 (village_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_B3C77447A76ED395 (user_id), INDEX IDX_B3C77447FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE login_tracker (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_BC4E34D2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_F62F176F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE town (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, district_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_4CE6C7A4F92F3E70 (country_id), INDEX IDX_4CE6C7A498260155 (region_id), INDEX IDX_4CE6C7A4B08FA272 (district_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, country_code VARCHAR(255) DEFAULT NULL, sms_price VARCHAR(255) DEFAULT NULL, voice_price VARCHAR(255) DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_5373C9665E237E06 (name), UNIQUE INDEX UNIQ_5373C966F026BB7C (country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE province (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_4ADAD40BF92F3E70 (country_id), INDEX IDX_4ADAD40B98260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE locality (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, province_id INT DEFAULT NULL, district_id INT DEFAULT NULL, town_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_E1D6B8E6F92F3E70 (country_id), INDEX IDX_E1D6B8E698260155 (region_id), INDEX IDX_E1D6B8E6E946114A (province_id), INDEX IDX_E1D6B8E6B08FA272 (district_id), INDEX IDX_E1D6B8E675E23604 (town_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE village (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, district_id INT DEFAULT NULL, town_id INT DEFAULT NULL, locality_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_4E6C7FAAF92F3E70 (country_id), INDEX IDX_4E6C7FAA98260155 (region_id), INDEX IDX_4E6C7FAAB08FA272 (district_id), INDEX IDX_4E6C7FAA75E23604 (town_id), INDEX IDX_4E6C7FAA88823A92 (locality_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE district (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, region_id INT DEFAULT NULL, province_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_31C15487F92F3E70 (country_id), INDEX IDX_31C1548798260155 (region_id), INDEX IDX_31C15487E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_credit_detail ADD CONSTRAINT FK_F1BE33A2A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE device_identity ADD CONSTRAINT FK_E4A1B0E4A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479727ACA70 FOREIGN KEY (parent_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A647998260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479B08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A647975E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64795E0D5582 FOREIGN KEY (village_id) REFERENCES village (id)');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447FE54D947 FOREIGN KEY (group_id) REFERENCES fos_user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE login_tracker ADD CONSTRAINT FK_BC4E34D2A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE town ADD CONSTRAINT FK_4CE6C7A4F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE town ADD CONSTRAINT FK_4CE6C7A498260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE town ADD CONSTRAINT FK_4CE6C7A4B08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE province ADD CONSTRAINT FK_4ADAD40BF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE province ADD CONSTRAINT FK_4ADAD40B98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE locality ADD CONSTRAINT FK_E1D6B8E6F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE locality ADD CONSTRAINT FK_E1D6B8E698260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE locality ADD CONSTRAINT FK_E1D6B8E6E946114A FOREIGN KEY (province_id) REFERENCES province (id)');
        $this->addSql('ALTER TABLE locality ADD CONSTRAINT FK_E1D6B8E6B08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE locality ADD CONSTRAINT FK_E1D6B8E675E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE village ADD CONSTRAINT FK_4E6C7FAAF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE village ADD CONSTRAINT FK_4E6C7FAA98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE village ADD CONSTRAINT FK_4E6C7FAAB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE village ADD CONSTRAINT FK_4E6C7FAA75E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE village ADD CONSTRAINT FK_4E6C7FAA88823A92 FOREIGN KEY (locality_id) REFERENCES locality (id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C15487F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C1548798260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C15487E946114A FOREIGN KEY (province_id) REFERENCES province (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447FE54D947');
        $this->addSql('ALTER TABLE user_credit_detail DROP FOREIGN KEY FK_F1BE33A2A76ED395');
        $this->addSql('ALTER TABLE device_identity DROP FOREIGN KEY FK_E4A1B0E4A76ED395');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479727ACA70');
        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447A76ED395');
        $this->addSql('ALTER TABLE login_tracker DROP FOREIGN KEY FK_BC4E34D2A76ED395');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A647998260155');
        $this->addSql('ALTER TABLE town DROP FOREIGN KEY FK_4CE6C7A498260155');
        $this->addSql('ALTER TABLE province DROP FOREIGN KEY FK_4ADAD40B98260155');
        $this->addSql('ALTER TABLE locality DROP FOREIGN KEY FK_E1D6B8E698260155');
        $this->addSql('ALTER TABLE village DROP FOREIGN KEY FK_4E6C7FAA98260155');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C1548798260155');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A647975E23604');
        $this->addSql('ALTER TABLE locality DROP FOREIGN KEY FK_E1D6B8E675E23604');
        $this->addSql('ALTER TABLE village DROP FOREIGN KEY FK_4E6C7FAA75E23604');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479F92F3E70');
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176F92F3E70');
        $this->addSql('ALTER TABLE town DROP FOREIGN KEY FK_4CE6C7A4F92F3E70');
        $this->addSql('ALTER TABLE province DROP FOREIGN KEY FK_4ADAD40BF92F3E70');
        $this->addSql('ALTER TABLE locality DROP FOREIGN KEY FK_E1D6B8E6F92F3E70');
        $this->addSql('ALTER TABLE village DROP FOREIGN KEY FK_4E6C7FAAF92F3E70');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C15487F92F3E70');
        $this->addSql('ALTER TABLE locality DROP FOREIGN KEY FK_E1D6B8E6E946114A');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C15487E946114A');
        $this->addSql('ALTER TABLE village DROP FOREIGN KEY FK_4E6C7FAA88823A92');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A64795E0D5582');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479B08FA272');
        $this->addSql('ALTER TABLE town DROP FOREIGN KEY FK_4CE6C7A4B08FA272');
        $this->addSql('ALTER TABLE locality DROP FOREIGN KEY FK_E1D6B8E6B08FA272');
        $this->addSql('ALTER TABLE village DROP FOREIGN KEY FK_4E6C7FAAB08FA272');
        $this->addSql('DROP TABLE fos_user_group');
        $this->addSql('DROP TABLE fos_user_user');
        $this->addSql('DROP TABLE user_credit_detail');
        $this->addSql('DROP TABLE device_identity');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE fos_user_user_group');
        $this->addSql('DROP TABLE login_tracker');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE town');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE province');
        $this->addSql('DROP TABLE locality');
        $this->addSql('DROP TABLE village');
        $this->addSql('DROP TABLE district');
    }
}
